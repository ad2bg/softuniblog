const accordionsStateStorageKey = 'accordions';

// SET ACCORDIONS' ACTIVE SECTIONS
// as a stringified array in the sessionStorage
function setAccordions(accordionId, sectionId) {
    let accordions = JSON.parse(sessionStorage.getItem(accordionsStateStorageKey));
    if (null === accordions) accordions = {};

    if (accordions[accordionId] === sectionId) {
        delete accordions[accordionId];
    } else {
        accordions[accordionId] = sectionId;
    }

    if ($.isEmptyObject(accordions)) {
        sessionStorage.removeItem(accordionsStateStorageKey);
    } else {
        sessionStorage.setItem(accordionsStateStorageKey, JSON.stringify(accordions));
    }
}

// ON DOCUMENT READY
$(function () {
    // OPEN ACCORDIONS
    let accordions = JSON.parse(sessionStorage.getItem(accordionsStateStorageKey));
    if (null !== accordions){
        for (let a in accordions){
            $('#' + accordions[a]).addClass('show');
        }
    }
});
