<?php

namespace SoftUniBlogBundle\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping;

/**
 * Class MyRepository
 * The other repositories extend this one, in order to use the constructor ready-made;
 * @package SoftUniBlogBundle\Repository
 */
class MyRepository extends \Doctrine\ORM\EntityRepository
{
    // CONSTRUCTOR

    /**
     * MyRepository constructor.
     * @param EntityManagerInterface $em
     * @throws \ReflectionException
     */
    public function __construct(EntityManagerInterface $em)
    {
        $reflectionClass = new \ReflectionClass($this);
        $repoName = $reflectionClass->getName();
        [$bundle, $_, $selfName] = explode('\\', $repoName);
        $entityName = $bundle . '\\Entity\\' . ucfirst(substr($selfName, 0, -10));
        //dump($entityName);
        //exit;
        /** @var Mapping\ClassMetadata $classMetadata */
        $classMetadata = new Mapping\ClassMetadata($entityName);
        parent::__construct($em, $classMetadata);
    }

    ///  EXTENDING THE CONSTRUCTOR:
    ///
    /// Starting point:
    ///
    /// class someRepository extends MyRepository
    ///    ...
    ///
    ///    /**
    ///     * @param Doctrine\ORM\EntityManagerInterface $em
    ///     */
    ///    public function __construct(EntityManagerInterface $em)
    ///    {
    ///        parent::__construct($em);
    ///    }
    ///
    ///    ...
    ///
    ///


    // SAVE

    /**
     * @param $entity
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save($entity): void
    {
        $this->_em->persist($entity);
        $this->_em->flush();
    }


    // MERGE

    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function merge($entity): void
    {
        $this->_em->merge($entity);
        $this->_em->flush();
    }
}