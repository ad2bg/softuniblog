<?php

namespace SoftUniBlogBundle\Service\Weather;

class WeatherService implements WeatherServiceInterface
{
    private $sessionKey = 'weather';
    private $types = ['snow', 'sky', 'beach'];

    /**
     * @var string
     */
    private $wx;

    public function __construct()
    {
        $this->wx = $_SESSION[$this->sessionKey] ?? $this->types[1];
    }

    public function startSnowing(): void
    {
        $this->changeWx($this->types[0]);
    }

    public function goToTheSky()
    {
        $this->changeWx($this->types[1]);
    }

    public function goToTheBeach()
    {
        $this->changeWx($this->types[2]);
    }

    private function changeWx(string $type)
    {
        $_SESSION[$this->sessionKey] = $type;
        $this->wx = $type;
    }

    /**
     * @return bool
     */
    public function isSnowing(): bool
    {
        return $this->wx === $this->types[0];
    }

    /**
     * @return bool
     */
    public function isForTheSky(): bool
    {
        return $this->wx === $this->types[1];
    }

    /**
     * @return bool
     */
    public function isForTheBeach(): bool
    {
        return $this->wx === $this->types[2];
    }
}