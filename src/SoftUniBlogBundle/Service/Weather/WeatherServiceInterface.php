<?php

namespace SoftUniBlogBundle\Service\Weather;

interface WeatherServiceInterface
{
    public function startSnowing();

    public function goToTheSky();

    public function goToTheBeach();

    public function isSnowing(): bool;

    public function isForTheSky(): bool;

    public function isForTheBeach(): bool;
}