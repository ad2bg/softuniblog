<?php

namespace SoftUniBlogBundle\Service\Role;

use SoftUniBlogBundle\Repository\RoleRepository;

/**
 * Class RoleService
 * @package SoftUniBlogBundle\Service\Role
 */
class RoleService implements RoleServiceInterface
{
    /**
     * @var RoleRepository
     */
    private $roleRepository;


    // CONSTRUCTOR

    /**
     * RoleService constructor.
     * @param RoleRepository $roleRepository
     */
    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }



    // GET ONE BY

    /**
     * @param array $kvpArray
     * @return null|object
     */
    public function getOneBy(array $kvpArray)
    {
        return $this->roleRepository->findOneBy($kvpArray);
    }
}