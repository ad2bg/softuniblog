<?php

namespace SoftUniBlogBundle\Service\Role;

interface RoleServiceInterface
{

    public function getOneBy(array $array);
}