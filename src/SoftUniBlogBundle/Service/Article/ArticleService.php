<?php

namespace SoftUniBlogBundle\Service\Article;


use SoftUniBlogBundle\Repository\ArticleRepository;

class ArticleService implements ArticleServiceInterface
{
    /**
     * @var ArticleRepository
     */
    private $articleRepository;

    public function __construct(ArticleRepository $articleRepository)
    {
        $this->articleRepository = $articleRepository;
    }

    public function getRepo(){
        return $this->articleRepository;
    }
}