<?php

namespace SoftUniBlogBundle\Service\Mail;


interface MailInterface
{
    public function send();
}