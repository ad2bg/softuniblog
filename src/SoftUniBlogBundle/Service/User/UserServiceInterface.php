<?php

namespace SoftUniBlogBundle\Service\User;

use SoftUniBlogBundle\Entity\User;

interface UserServiceInterface
{
    public function register(User $user): void;

    public function getOneById($id): ?User;

    public function hashPassword($user): void;

    public function doChangeUserPassword($user, $password): void;

    public function save($user): void;

    public function merge($user): void;
}