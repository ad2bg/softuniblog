<?php

namespace SoftUniBlogBundle\Service\User;

use SoftUniBlogBundle\Entity\Role;
use SoftUniBlogBundle\Entity\User;
use SoftUniBlogBundle\Repository\userRepository;
use SoftUniBlogBundle\Service\Role\RoleServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class UserService
 * @package SoftUniBlogBundle\Service\User
 */
class UserService implements UserServiceInterface
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var RoleServiceInterface
     */
    private $roleService;

    /**
     * @var ContainerInterface
     */
    private $container;

    // CONSTRUCTOR

    /**
     * UserService constructor.
     * @param userRepository $userRepository
     * @param RoleServiceInterface $roleService
     * @param ContainerInterface $container
     */
    public function __construct(
        UserRepository $userRepository,
        RoleServiceInterface $roleService,
        ContainerInterface $container)
    {
        $this->userRepository = $userRepository;
        $this->roleService = $roleService;
        $this->container = $container;
    }


    // REGISTER

    /**
     * @param $user
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function register($user): void
    {
        $this->addRole('ROLE_USER', $user);
        $this->hashPassword($user);
        $this->userRepository->save($user);
    }


    // GET ONE BY ID

    /**
     * @param $id
     * @return null|object|User
     */
    public function getOneById($id): ?User
    {
        return $this->userRepository->find($id);
    }


    // GET ONE BY

    /**
     * @param array $kvpArray
     * @return null|object
     */
    public function getOneBy($kvpArray)
    {
        return $this->userRepository->findOneBy($kvpArray);
    }


    // ADD ROLE

    /**
     * @param string $roleName
     * @param User $user
     */
    private function addRole($roleName, $user): void
    {
        /** @var Role $role */
        $role = $this->roleService->getOneBy(['name' => $roleName]);
        $user->addRole($role);
    }


    // HASH PASSWORD

    /**
     * @param User $user
     */
    public function hashPassword($user): void
    {
        $rawPassword = $user->getPassword();
        if (is_array($rawPassword)) {
            $rawPassword = array_shift($rawPassword);
        }
        $hashedPassword = $this->container
            ->get('security.password_encoder')
            ->encodePassword($user, $rawPassword);
        $user->setPassword($hashedPassword);
    }


    // DO CHANGE USER PASSWORD

    /**
     * @param User $user
     * @param string $password
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function doChangeUserPassword($user, $password): void
    {
        $user->setPassword($password);
        $this->hashPassword($user);
        $this->merge($user);
    }



    // SAVE

    /**
     * @param $user
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save($user): void
    {
        $this->userRepository->save($user);
    }


    // MERGE

    /**
     * @param $user
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function merge($user): void
    {
        $this->userRepository->merge($user);
    }
}