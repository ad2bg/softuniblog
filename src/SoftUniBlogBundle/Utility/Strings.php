<?php

namespace SoftUniBlogBundle\Utility;

class Strings
{
    /**
     * @param string $aString
     * @param int $cutAt
     * @return string The shortened version of the string ending with ...
     */
    public static function shorten(string $aString, int $cutAt): string
    {
        return strlen($aString) > $cutAt ? substr($aString, 0, $cutAt) . '...' : $aString;
    }
}
