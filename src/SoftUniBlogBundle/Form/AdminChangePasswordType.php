<?php

namespace SoftUniBlogBundle\Form;

use Doctrine\ORM\EntityRepository;
use SoftUniBlogBundle\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdminChangePasswordType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        //$em = $options['entityManager'];

        // USER
        $builder->add('user', EntityType::class, [
            'class' => User::class,
            'required' => true,
            'placeholder' => '',
            //'choice_label' => 'email',
            'query_builder' => function (EntityRepository $er){
                return $er->createQueryBuilder('u')
                    ->orderBy('u.email', 'ASC');
            },
        ]);

        // PASSWORD
        $builder->add('password', RepeatedType::class, [
            'type' => PasswordType::class,
            'invalid_message' => 'The password fields must match.',
            'options' => ['attr' => ['class' => 'password-field',]],
            'required' => true,
            'first_options' => ['label' => 'Password', 'attr' => ['placeholder' => 'Password',]],
            'second_options' => ['label' => 'Repeat Password', 'attr' => ['placeholder' => 'Repeat Password',]],
        ]);
    }

    public function configureOptions(OptionsResolver $resolver):void
    {
        $resolver->setDefaults([
            //'data_class' => SomeEntity::class,
            //'entityManager' => null,
        ]);
    }
}
