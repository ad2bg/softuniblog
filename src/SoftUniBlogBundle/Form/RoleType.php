<?php

namespace SoftUniBlogBundle\Form;

use SoftUniBlogBundle\Entity\Role;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RoleType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $protectedRole = $options['protectedRole'];

        $builder
            ->add('name', null, [
                'attr' => [
                    'autofocus' => !$protectedRole,
                    'placeholder' => 'ROLE_...',
                    'data-toggle' => 'tooltip',
                    'title' => 'The Role name must start with ROLE_ and contain only ' .
                        'uppercase latin characters, underscores and digits.',
                    'disabled' => $protectedRole,
                ],
            ])
            ->add('users', null, [
                'attr' => [
                    'size' => 10,
                    'autofocus' => $protectedRole,
                    'data-toggle' => 'tooltip',
                    'title' => 'Please select the users belonging to this Role.'
                ],
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => Role::class,'protectedRole' => null,]);
    }
}
