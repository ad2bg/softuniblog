<?php

namespace SoftUniBlogBundle\Form;

use Doctrine\ORM\EntityRepository;
use SoftUniBlogBundle\Entity\Category;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CategoryType extends AbstractType
{
    private $pathSeparator = '/';
    private $pathSeparatorFormatted = ' >> ';

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options):void
    {
        $parentsQueryBuilder = function (EntityRepository $er){
            return $er->createQueryBuilder('c')
                ->orderBy('c.path', 'ASC');
        };

        $builder
            ->add('parent', null,[
                'label' => 'Parent Category',
                'query_builder' => $parentsQueryBuilder,
                'placeholder' => $this->pathSeparatorFormatted,
                'choice_label' => 'getNameTree',
            ])
            ->add('name')
            ->add('description')
        ; //  name path getNameWithSpaces getPathFormatted getNameTree
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver):void
    {
        $resolver->setDefaults(array(
            'data_class' => Category::class
        ));
    }
}
