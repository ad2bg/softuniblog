<?php

namespace SoftUniBlogBundle\Form;

use Doctrine\ORM\EntityRepository;
use SoftUniBlogBundle\Entity\Message;
use SoftUniBlogBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MessageType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options):void
    {
        $id = $options['id'];

        $builder
            ->add('recipient', null, [
                'attr' => ['autofocus' => true],
                'label' => 'To',
                'class' => User::class,
                'query_builder' => function (EntityRepository $er) use ($id) {
                    return $er->createQueryBuilder('u')
                        ->where('u.id != :id')
                        ->setParameter('id', $id )
                        ->orderBy('u.fullName', 'ASC');
                },
                'choice_label' => 'getNameAndArticlesCount',
            ])

            ->add('title')

            ->add('content',null,[
                'attr' => ['rows' => 10],
            ])
        ;
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver):void
    {
        $resolver->setDefaults(array(
            'data_class' => Message::class,
            'id' => null,
        ));
    }
}
