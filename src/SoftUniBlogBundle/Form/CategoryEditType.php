<?php

namespace SoftUniBlogBundle\Form;

use Doctrine\ORM\EntityRepository;
use SoftUniBlogBundle\Entity\Category;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CategoryEditType extends AbstractType
{

    private $pathSeparator = '/';
    private $pathSeparatorFormatted = ' >> ';

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options):void
    {
        $em = $options['entityManager'];
        $id = $options['id'];
        $existingPath = $options['existingPath'];

        $possibleParentsQueryBuilder = function (EntityRepository $er) use ($existingPath) {
            return $er->createQueryBuilder('c')
                ->where('c.path NOT LIKE :pathStartsWith')
                ->setParameter('pathStartsWith', $existingPath . '%')
                ->orderBy('c.path', 'ASC');
        };

        $parentFieldOptions = [
            'class' => Category::class,
            'label' => 'Parent Category',
            'query_builder' => $possibleParentsQueryBuilder,
            'required' => false,
            'placeholder' => $this->pathSeparatorFormatted,
            'choice_label' => 'getPathFormatted',
        ]; //  name path getNameWithSpaces getPathFormatted getNameTree

        if (null !== $id) {
            $parentFieldOptions['choice_value'] = 'id';
            $parentFieldOptions['attr'] = ['class' => 'id'];
            $parentFieldOptions['data'] = $em->getReference(Category::class, $id);
        }

        $builder
            ->add('parent', EntityType::class, $parentFieldOptions)
            ->add('name')
            ->add('description');
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver):void
    {
        $resolver->setDefaults(array(
            'data_class' => Category::class,
            'entityManager' => null,
            'existingPath' => null,
            'id' => null,
        ));
    }
}
