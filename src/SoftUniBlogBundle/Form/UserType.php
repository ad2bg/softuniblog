<?php

namespace SoftUniBlogBundle\Form;

use SoftUniBlogBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {

        // EMAIL
        $builder->add('email', EmailType::class, [
            'attr' => ['placeholder' => 'Email', 'autofocus' => true],
        ]);

        // FULL NAME
        $builder->add('fullName', TextType::class, [
            'label' => 'Full Name',
            'attr' => ['placeholder' => 'Full Name'],
            'constraints' => [
                new NotBlank(),
                new Length(['min' => 5, 'max' => 255])
            ],
        ]);

        // PHOTO
        $folder = $options['user_photos_directory'];
        $builder->add('photo', FileType::class, []);
        $builder->get('photo')
            ->addModelTransformer(new CallbackTransformer(
                function (?string $photoAsString) use ($folder) {
                    // transform the photo filename to a file object
                    if (null === $photoAsString) {
                        return null;
                    }
                    return new UploadedFile($folder . $photoAsString, $photoAsString);
                },
                function (?UploadedFile $photoFile) {
                    // transform a file object to a string filename
                    if (null === $photoFile) {
                        return null;
                    }
                    return $photoFile->getPathname();
                }
            ));

        // PASSWORD
        $builder->add('password', RepeatedType::class, [
            'type' => PasswordType::class,
            'invalid_message' => 'The password fields must match.',
            'options' => ['attr' => ['class' => 'password-field',]],
            'required' => true,
            'first_options' => ['label' => 'Password', 'attr' => ['placeholder' => 'Password',]],
            'second_options' => ['label' => 'Repeat Password', 'attr' => ['placeholder' => 'Repeat Password',]],
        ]);

        // TERMS
        $builder->add('termsAgreed', CheckboxType::class, [
            'mapped' => false,
//            'required' => true,
//            'invalid_message' => 'You must accept the Terms Of Service.',
            'constraints' => [
                new IsTrue(['message' => 'You must accept the Terms Of Service.'])
            ],
            'label' => 'I agree to the Terms Of Service.'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(array(
            'data_class' =>  User::class,
        ));
        $resolver->setRequired('user_photos_directory');
    }

}
