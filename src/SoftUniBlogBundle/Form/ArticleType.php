<?php

namespace SoftUniBlogBundle\Form;

use Doctrine\ORM\EntityRepository;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use SoftUniBlogBundle\Entity\Article;
use SoftUniBlogBundle\Entity\Category;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArticleType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $em = $options['entityManager'];
        $categoryId = $options['categoryId'];

        $categoryQueryBuilder = function (EntityRepository $er) {
            $qb = $er->createQueryBuilder('c')
                ->select('c')
                ->orderBy('c.path', 'ASC');
            return $qb;
        };

        $categoryOptions = [
            'class' => Category::class,
            'required' => false,
            'placeholder' => '/',
            'choice_label' => 'path',
            'query_builder' => $categoryQueryBuilder,
        ];

        if (null !== $categoryId) {
            $categoryOptions['choice_value'] = 'id';
            $categoryOptions['attr'] = ['class' => 'id'];
            $categoryOptions['data'] = $em->getReference(Category::class, $categoryId);
        }

        $tagsQueryBuilder = function (EntityRepository $er) {
            $qb = $er->createQueryBuilder('t')
                ->select('t')
                ->orderBy('t.name', 'ASC');
            return $qb;
        };

        $builder
            ->add('category', EntityType::class, $categoryOptions)
            ->add('tags', null, [
                'attr' => ['size' => 10],
                'query_builder' => $tagsQueryBuilder,
            ])
            ->add('imageUrl', UrlType::class, ['required' => false,])
            ->add('title', TextType::class)
            ->add('summary', CKEditorType::class, ['config' => [],])
            ->add('content', CKEditorType::class, ['attr' => ['required' => true,], 'config' => [],])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(array(
            'data_class' => Article::class,
            'entityManager' => null,
            'categoryId' => null,
        ));
    }
}
