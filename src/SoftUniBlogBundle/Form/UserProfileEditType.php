<?php

namespace SoftUniBlogBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class UserProfileEditType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        // EMAIL
        $builder->add('email', EmailType::class, [
            'attr' => ['placeholder' => 'Email', 'autofocus' => true],
        ]);

        // FULL NAME
        $builder->add('fullName', TextType::class, [
            'label' => 'Full Name',
            'attr' => ['placeholder' => 'Full Name', ],
        ]);
    }
}
