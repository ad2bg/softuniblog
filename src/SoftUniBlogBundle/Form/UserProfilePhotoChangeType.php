<?php

namespace SoftUniBlogBundle\Form;

use SoftUniBlogBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserProfilePhotoChangeType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options):void
    {
        $folder = $options['user_photos_directory'];

        $builder->add('photo', FileType::class, []);

        $builder->get('photo')
            ->addModelTransformer(new CallbackTransformer(
                function (?string $photoAsString) use ($folder) {
                    // transform the photo filename to a file object
                    if (null === $photoAsString) {
                        return null;
                    }
                    return new UploadedFile($folder . $photoAsString, $photoAsString);
                },
                function (?UploadedFile $photoFile) {
                    // transform a file object to a string filename
                    if (null === $photoFile) {
                        return null;
                    }
                    return $photoFile->getPathname();
                }
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver):void
    {
        $resolver->setDefaults(array(
            'data_class' => User::class
        ));
        $resolver->setRequired('user_photos_directory');
    }
}
