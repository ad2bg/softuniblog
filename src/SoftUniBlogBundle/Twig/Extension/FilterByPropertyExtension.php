<?php

namespace SoftUniBlogBundle\Twig\Extension;

use Doctrine\Common\Collections\ArrayCollection;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class FilterByPropertyExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return array(
            new TwigFilter('filterByProperty', array($this, 'filterByProperty')),
        );
    }

    /**
     * @param ArrayCollection|array $arr
     * @param string $getterName
     * @param $filterValue
     * @return array
     */
    public function filterByProperty($arr, $filterValue, string $getterName = 'getName'): array
    {
        $a = array_map(function ($e) use ($getterName) {
            return $e->$getterName();
        }, $arr->toArray());

        $a = array_filter($a, function ($i) use ($filterValue) {
            return $i === $filterValue;
        });
        return $a;
    }
}
