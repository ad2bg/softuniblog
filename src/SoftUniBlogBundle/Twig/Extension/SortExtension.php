<?php

namespace SoftUniBlogBundle\Twig\Extension;

use Doctrine\Common\Collections\ArrayCollection;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class SortExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return array(
            new TwigFilter('sortByProp', array($this, 'sortByProp')),
        );
    }

    /**
     * @param ArrayCollection|array $arr
     * @param string $methodName
     * @return array
     */
    public function sortByProp($arr, string $methodName = 'getName'): array
    {
        $a = array_map(function ($e) use ($methodName) {
            return $e->$methodName();
        }, $arr->toArray());
        asort($a);
        return $a;
    }
}