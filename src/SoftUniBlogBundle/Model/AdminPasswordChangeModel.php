<?php

namespace SoftUniBlogBundle\Model;

use SoftUniBlogBundle\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;

class AdminPasswordChangeModel
{
    /**
     * @Assert\NotBlank()
     * @var User
     */
    public $user;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(
     *     min="4",
     *     max="20",
     *     minMessage="The password should have {{ limit }} characters or more.",
     *     minMessage="The password should have {{ limit }} characters or less."
     * )
     * @var string
     */
    public $password;

}
