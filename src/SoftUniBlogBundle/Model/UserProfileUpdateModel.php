<?php

namespace SoftUniBlogBundle\Model;

use SoftUniBlogBundle\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;

class UserProfileUpdateModel
{
    /**
     * @Assert\NotBlank()
     * @Assert\Email()
     * @var string
     */
    public $email;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(
     *     min="5",
     *     max="255",
     *     minMessage="Your full name should have {{ limit }} characters or more.",
     *     minMessage="Your full name should have {{ limit }} characters or less."
     * )
     * @var string
     */
    public $fullName;

    public static function fromUser(User $user): self
    {
        $model = new self();
        $model->email = $user->getEmail();
        $model->fullName = $user->getFullName();
        return $model;
    }

}