<?php

namespace SoftUniBlogBundle\Model;

use Symfony\Component\Validator\Constraints as Assert;

class UserPasswordChangeModel
{
    /**
     * @Assert\NotBlank()
     * @Assert\Length(
     *     min="4",
     *     max="20",
     *     minMessage="Your password should have {{ limit }} characters or more.",
     *     minMessage="Your password should have {{ limit }} characters or less."
     * )
     * @var string
     */
    public $password;
}
