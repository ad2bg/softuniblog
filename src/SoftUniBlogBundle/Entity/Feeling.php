<?php

namespace SoftUniBlogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Feeling
 *
 * @ORM\Table(
 *     name="feelings",
 *     uniqueConstraints={@ORM\UniqueConstraint(columns={"user_id", "article_id"})}
 * )
 * @ORM\Entity(repositoryClass="SoftUniBlogBundle\Repository\FeelingRepository")
 */
class Feeling
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="type", type="smallint")
     */
    private $type;

    /**
     * Many Feelings have one User. This is the owning side.
     * @var User
     * @ORM\ManyToOne(targetEntity="User", inversedBy="feelings")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * Many Feelings have one Article. This is the owning side.
     * @var Article
     * @ORM\ManyToOne(targetEntity="Article", inversedBy="feelings")
     * @ORM\JoinColumn(name="article_id", referencedColumnName="id")
     */
    private $article;






//    GETTERS

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get type.
     *
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return Article
     */
    public function getArticle(): Article
    {
        return $this->article;
    }



//    SETTERS

    /**
     * Set type.
     *
     * @param int $type
     *
     * @return Feeling
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @param User $user
     * @return Feeling
     */
    public function setUser(User $user): Feeling
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @param Article $article
     * @return Feeling
     */
    public function setArticle(Article $article): Feeling
    {
        $this->article = $article;
        return $this;
    }


}
