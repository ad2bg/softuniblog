<?php

namespace SoftUniBlogBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="roles")
 * @ORM\Entity(repositoryClass="SoftUniBlogBundle\Repository\RoleRepository")
 */
class Role
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     * @Assert\NotBlank()
     * @Assert\Regex("/^ROLE_[_A-Z0-9]+$/")
     */
    private $name;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="SoftUniBlogBundle\Entity\User", mappedBy="roles")
     */
    private $users;


    // CONSTRUCTOR
    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    // TO STRING
    public function __toString()
    {
        return $this->name;
    }


    // GETTERS
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getRole(): ?string
    {
        return $this->getName();
    }

    public function getUsers()
    {
        return $this->users;
    }




    // SETTERS

    /**
     * @param $name
     * @return Role
     */
    public function setName($name): Role
    {
        $this->name = htmlspecialchars($name);
        return $this;
    }

    /**
     * @param User $user
     * @return Role
     */
    public function addUser(User $user): Role
    {
        $this->users[] = $user;
        $user->addRole($this);
        return $this;
    }

    /**
     * @param User $user
     * @return Role
     */
    public function removeUser(User $user): Role
    {
        $this->users->removeElement($user);
        $user->removeRole($this);
        return $this;
    }

}

