<?php

namespace SoftUniBlogBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use SoftUniBlogBundle\Model\UserProfileUpdateModel;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="SoftUniBlogBundle\Repository\userRepository")
 * @UniqueEntity(fields="email", message="This email address is already used.")
 */
class User implements UserInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Email(
     *     message = "The email {{ value }} is not a valid email.",
     * )
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     */
    private $email;

//    /**
//     * @Assert\NotBlank()
//     * @Assert\Length(min=1, max=4096)
//     */
    private $plainPassword;
    /**
     * @var string
     *
     * @Assert\Length(
     *     min = 1,
     *     max = 255,
     *     minMessage = "Your password must be at least {{ limit }} characters long.",
     *     maxMessage = "Your password cannot be longer than {{ limit }} characters.",
     * )
     *
     * @ORM\Column(name="password", type="string", length=255)
     */
    private $password; // TODO: fix min length = 4

    /**
     * @var string
     *
     * @Assert\Length(
     *     min = 5,
     *     max = 255,
     *     minMessage = "Your full name must be at least {{ limit }} characters long.",
     *     maxMessage = "Your full name cannot be longer than {{ limit }} characters.",
     * )
     *
     * @ORM\Column(name="fullName", type="string", length=255)
     */
    private $fullName;

    /**
     * @var string
     * @ORM\Column(name="photo", type="text", nullable=true)
     * @Assert\File(
     *     maxSize = "25Mi",
     *     mimeTypes={ "image/jpeg", "image/gif", "image/png" }
     *     )
     */
    private $photo;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="SoftUniBlogBundle\Entity\Article", mappedBy="author")
     */
    private $articles;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="SoftUniBlogBundle\Entity\Role", inversedBy="users")
     * @ORM\JoinTable(name="users_roles",
     *     joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id")}
     *     )
     */
    private $roles;

    /**
     * One User has many Feelings. This is the inverse side.
     * @var ArrayCollection|Feeling[]
     * @ORM\OneToMany(targetEntity="Feeling", mappedBy="user")
     */
    private $feelings;

    /**
     * @var ArrayCollection|Comment[]
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="user")
     */
    private $comments;

    /**
     * @var ArrayCollection|Message[]
     * @ORM\OneToMany(targetEntity="Message", mappedBy="sender")
     */
    private $sentMessages;

    /**
     * @var ArrayCollection|Message[]
     * @ORM\OneToMany(targetEntity="Message", mappedBy="recipient")
     */
    private $receivedMessages;


    // CONSTRUCTOR
    public function __construct()
    {
        $this->roles = new ArrayCollection();
        $this->articles = new ArrayCollection();
        $this->feelings = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->sentMessages = new ArrayCollection();
        $this->receivedMessages = new ArrayCollection();
    }

    // TO STRING
    public function __toString()
    {
        return $this->fullName . ' (' . $this->email . ')';
    }


    // GETTERS
    public function getId(): int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    public function getArticles()
    {
        return $this->articles;
    }


//    /**
//     * Note that this method MUST return an array; not a generator, etc.
//     * Otherwise the whole login system fails.
//     */
//    public function getRoles()
//    {
//        return $this->roles;
//    }


    /**
     * Note that this method MUST return an array; not a generator, etc.
     * Otherwise the whole login system fails.
     * @return array
     */
    public function getRoles(): array
    {
        $arrayOfStrings = [];
        foreach ($this->roles as $role) {
            /** @var Role $role */
            $arrayOfStrings[] = $role->getRole();
        }
        return $arrayOfStrings;
    }

    /**
     * @return \Generator
     */
    public function getRolesIds(): ?\Generator
    {
        foreach ($this->roles as $role) {
            /** @var Role $role */
            yield $role->getId();
        }
    }

    public function hasRole($role): bool
    {
        if (\is_string($role)) {
            return \in_array($role, $this->getRoles());
        }
        if (\is_int($role)) {
            return \in_array($role, iterator_to_array($this->getRolesIds()));
        }
        return $this->roles->contains($role);
    }

    public function isApiUser(): bool
    {
        return $this->hasRole('ROLE_API_USER');
    }

    public function isAdmin(): bool
    {
        return $this->hasRole('ROLE_ADMIN');
    }

    /**
     * @return ArrayCollection|Feeling[]
     */
    public function getFeelings()
    {
        return $this->feelings;
    }

    /**
     * @return ArrayCollection|Comment[]
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @return ArrayCollection|Message[]
     */
    public function getSentMessages()
    {
        return $this->sentMessages;
    }

    /**
     * @return ArrayCollection|Message[]
     */
    public function getReceivedMessages()
    {
        return $this->receivedMessages;
    }

    /**
     * Returns the salt that was originally used to encode the password.
     * This can return null if the password was not encoded using a salt.
     */
    public function getSalt()
    {
        // TODO: Implement getSalt() method.
//        throw new NotImplementedException(
//            '// TODO: Implement getSalt() method.');
    }

    /**
     * Returns the username used to authenticate the user.
     */
    public function getUsername(): ?string
    {
        return $this->email;
    }

    public function isAuthor(Article $article): bool
    {
        return $article->getAuthorId() === $this->getId();
    }

    public function hasFeeling(Article $article): bool
    {
        $feelings = $this->feelings;
        $feltArticleIds = [];
        foreach ($feelings as $feeling) {
            $feltArticleIds[] = $feeling->getArticle()->getId();
        }
        return in_array($article->getId(), $feltArticleIds);
    }

    /**
     * @return mixed
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function getNameAndArticlesCount():string
    {
        $name = $this->getFullName();
        $articlesCount = count($this->articles);
        return "$name -> $articlesCount articles";
    }


    // SETTERS

    public function setEmail(?string $email): User
    {
        $this->email = htmlspecialchars($email);
        return $this;
    }

    public function setPassword(?string $password): User
    {
        $this->password = $password;
        return $this;
    }

    public function setFullName(?string $fullName): User
    {
        $this->fullName = htmlspecialchars($fullName);
        return $this;
    }

    public function setPhoto(?string $photo): User
    {
        $this->photo = null === $photo ? null : htmlspecialchars($photo);
        return $this;
    }

    /**
     * @param ArrayCollection|Comment[] $comments
     * @return User
     */
    public function setComments($comments): User
    {
        $this->comments = $comments;
        return $this;
    }

    /**
     * @param ArrayCollection|Message[] $sentMessages
     */
    public function setSentMessages($sentMessages): void
    {
        $this->sentMessages = $sentMessages;
    }

    /**
     * @param ArrayCollection|Message[] $receivedMessages
     */
    public function setReceivedMessages($receivedMessages): void
    {
        $this->receivedMessages = $receivedMessages;
    }

    public function addArticle(Article $article): User
    {
        $this->articles[] = $article;
        return $this;
    }

    public function addRole(Role $role): User
    {
        if (!$this->hasRole($role)) {
            $this->roles[] = $role;
        }
        return $this;
    }

    public function removeRole(Role $role): User
    {
        if ($this->hasRole($role)) {
            $this->roles->removeElement($role);
        }
        return $this;
    }

//    /**
//     * @param ArrayCollection $articles
//     * @return User
//     */
//    public function setArticles(ArrayCollection $articles): User
//    {
//        $this->articles = $articles;
//        return $this;
//    }

    /**
     * @param ArrayCollection $roles
     * @return User
     */
    public function setRoles(ArrayCollection $roles): User
    {
        $this->roles = $roles;
        return $this;
    }

    /**
     * @param ArrayCollection|Feeling[] $feelings
     * @return User
     */
    public function setFeelings($feelings): User
    {
        $this->feelings = $feelings;
        return $this;
    }

    /**
     * @param Message $message
     * @return User
     */
    public function addSentMessage(Message $message): User
    {
        $this->sentMessages[] = $message;
        return $this;
    }


    /**
     * @param Message $message
     * @return User
     */
    public function addReceivedMessage(Message $message): User
    {
        $this->receivedMessages[] = $message;
        return $this;
    }

    /**
     * @param mixed $plainPassword
     * @return User
     */
    public function setPlainPassword($plainPassword): User
    {
        $this->plainPassword = $plainPassword;
        return $this;
    }


    /**
     * Removes sensitive data from the user.
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials(): User
    {
        // TODO: Implement eraseCredentials() method.
//        throw new NotImplementedException(
//            'TODO: Implement eraseCredentials() method.');
        return $this;
    }


    /**
     * @param UserProfileUpdateModel $model
     * @return User
     */
    public function setFromProfileUpdateModel(UserProfileUpdateModel $model): User
    {
        return $this->setEmail($model->email)->setFullName($model->fullName);
    }
}
