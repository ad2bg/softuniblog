<?php

namespace SoftUniBlogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use SoftUniBlogBundle\Utility\Strings;

/**
 * @ORM\Table(name="messages")
 * @ORM\Entity(repositoryClass="SoftUniBlogBundle\Repository\MessageRepository")
 */
class Message
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     */
    private $content;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timestamp", type="datetime")
     */
    private $timestamp;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User", inversedBy="sentMessages")
     * @ORM\JoinColumn(name="sender_id", referencedColumnName="id")
     */
    private $sender;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User", inversedBy="receivedMessages")
     * @ORM\JoinColumn(name="recipient_id", referencedColumnName="id")
     */
    private $recipient;


    // CONSTRUCTOR

    public function __construct()
    {
        $this->setTimestamp(new \DateTime('now'));
    }


    // TO STRING
    public function __toString()
    {
        $title = $this->title;
        $result = '' . ((null !== $title && '' !== trim($title)) ? $title : $this->content);
        return '' . Strings::shorten($result,250);
    }


    // GETTERS

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @return null|string
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @return \DateTime
     */
    public function getTimestamp(): \DateTime
    {
        return $this->timestamp;
    }

    /**
     * @return null|User
     */
    public function getSender(): ?User
    {
        return $this->sender;
    }

    /**
     * @return null|User
     */
    public function getRecipient(): ?User
    {
        return $this->recipient;
    }


    // SETTERS

    /**
     * @param string|null $title
     * @return Message
     */
    public function setTitle($title = null): Message
    {
        $this->title = htmlspecialchars($title);

        return $this;
    }

    /**
     * @param string $content
     * @return Message
     */
    public function setContent($content): Message
    {
        $this->content = htmlspecialchars($content);
        return $this;
    }

    /**
     * @param \DateTime $timestamp
     * @return Message
     */
    public function setTimestamp($timestamp): Message
    {
        $this->timestamp = $timestamp;

        return $this;
    }

    /**
     * @param User $sender
     * @return Message
     */
    public function setSender(User $sender): Message
    {
        $this->sender = $sender;
        $sender->addSentMessage($this);
        return $this;
    }

    /**
     * @param User $recipient
     * @return Message
     */
    public function setRecipient(User $recipient): Message
    {
        $this->recipient = $recipient;
        return $this;
    }

}
