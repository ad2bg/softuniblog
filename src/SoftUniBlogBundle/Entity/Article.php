<?php

namespace SoftUniBlogBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Nelmio\ApiDocBundle\Annotation\Model;
use SoftUniBlogBundle\Utility\Strings;
use Swagger\Annotations as SWG;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="articles")
 * @ORM\Entity(repositoryClass="SoftUniBlogBundle\Repository\ArticleRepository")
 * @Serializer\ExclusionPolicy("all")
 * @SWG\Response(response=200, @Model(type=Article::class))
 */
class Article
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     * @Serializer\XmlElement(cdata=false)
     * @SWG\Property(description="The unique identifier of the article.")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     * @Assert\NotBlank()
     * @Serializer\Expose()
     * @Serializer\Type("string")
     * @Serializer\XmlElement(cdata=false)
     * @SWG\Property(type="string", maxLength=255)
     */
    private $title;

    /**
     * @var string
     * @ORM\Column(name="summary", type="text")
     * @Assert\NotBlank()
     */
    private $summary;

    /**
     * @var string
     * @ORM\Column(name="content", type="text")
     * @Assert\NotBlank()
     */
    private $content;

    /**
     * @var \DateTime
     * @ORM\Column(name="dateAdded", type="datetime")
     */
    private $dateAdded;


    /**
     * @var int
     * @ORM\Column(name="authorId", type="integer")
     */
    private $authorId;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="SoftUniBlogBundle\Entity\User", inversedBy="articles")
     * @ORM\JoinColumn(name="authorId", referencedColumnName="id")
     * @SWG\Property(ref=@Model(type=User::class))
     */
    private $author;

    /**
     * @var string
     * @ORM\Column(name="imageUrl", type="text", nullable=true)
     * @Assert\Url()
     */
    private $imageUrl;


    /**
     * @var int
     * @ORM\Column(name="viewCount", type="integer")
     */
    private $viewCount;

    /**
     * @var ArrayCollection|Feeling[]
     * @ORM\OneToMany(targetEntity="Feeling", mappedBy="article")
     */
    private $feelings;

    /**
     * @var ArrayCollection|Comment[]
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="article")
     */
    private $comments;

    /**
     * @var ArrayCollection|Tag[]
     * @ORM\ManyToMany(targetEntity="Tag", inversedBy="articles")
     * @ORM\JoinTable(name="articles_tags")
     */
    private $tags;


    /**
     * @var ?Category
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="articles")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", nullable=true)
     */
    private $category;


    // CONSTRUCTOR
    public function __construct()
    {
        $this->dateAdded = new \DateTime('now');
        $this->feelings = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->tags = new ArrayCollection();
    }

    // TO STRING
    public function __toString()
    {
        return Strings::shorten($this->title, 100);
    }



    //GETTERS

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title??'';
    }

    /**
     * @return string
     */
    public function getSummary(): string
    {
        return $this->summary??'';
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content??'';
    }

    /**
     * @return \DateTime
     */
    public function getDateAdded(): \DateTime
    {
        return $this->dateAdded;
    }

    /**
     * @return int
     */
    public function getAuthorId(): int
    {
        return $this->authorId;
    }

    /**
     * @return User
     */
    public function getAuthor(): User
    {
        return $this->author;
    }

    /**
     * @return string
     */
    public function getImageUrl(): string
    {
        return $this->imageUrl??'';
    }

    /**
     * @return int
     */
    public function getViewCount(): int
    {
        return $this->viewCount;
    }

    /**
     * @return array
     */
    public function getFeelings(): array
    {
        return array_count_values(array_map(function (Feeling $f) {
            return $f->getType();
        }, $this->feelings->toArray()));
    }

    /**
     * @return int
     */
    public function getLikesCount(): int
    {
        return $this->getFeelings()[1] ?? 0;
    }

    /**
     * @return int
     */
    public function getDislikesCount(): int
    {
        return $this->getFeelings()[-1] ?? 0;
    }

    /**
     * @return ArrayCollection|Comment[]
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @return ArrayCollection|Tag[]
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @return null|Category
     */
    public function getCategory(): ?Category
    {
        return $this->category;
    }




    // SETTERS

    /**
     * @param string $title
     * @return Article
     */
    public function setTitle($title): Article
    {
        $this->title = htmlspecialchars($title);
        return $this;
    }

    /**
     * @param string $summary
     * @return Article
     */
    public function setSummary($summary): Article
    {
        $this->summary = $summary;
        return $this;
    }

    /**
     * @param string $content
     * @return Article
     */
    public function setContent($content): Article
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @param \DateTime $dateAdded
     * @return Article
     */
    public function setDateAdded($dateAdded): Article
    {
        $this->dateAdded = $dateAdded;
        return $this;
    }

    /**
     * @param int $authorId
     * @return Article
     */
    public function setAuthorId($authorId): Article
    {
        $this->authorId = $authorId;
        return $this;
    }

    /**
     * @param User $author
     * @return Article
     */
    public function setAuthor($author = null): Article
    {
        $this->author = $author;
        return $this;
    }

    /**
     * @param string $imageUrl
     * @return Article
     */
    public function setImageUrl($imageUrl): Article
    {
        $this->imageUrl = htmlspecialchars($imageUrl);
        return $this;
    }

    /**
     * @param int $viewCount
     * @return Article
     */
    public function setViewCount($viewCount): Article
    {
        $this->viewCount = $viewCount;
        return $this;
    }

    /**
     * @param ArrayCollection|Feeling[] $feelings
     * @return Article
     */
    public function setFeelings($feelings): Article
    {
        $this->feelings = $feelings;
        return $this;
    }

    /**
     * @param ArrayCollection|Comment[] $comments
     * @return Article
     */
    public function setComments($comments): Article
    {
        $this->comments = $comments;
        return $this;
    }

    /**
     * @param ArrayCollection|Tag[] $tags
     * @return Article
     */
    public function setTags($tags): Article
    {
        $this->tags = $tags;
        return $this;
    }

    /**
     * @param Tag $tag
     * @return Article
     */
    public function addTag(Tag $tag): Article
    {
        $this->tags[] = $tag;
        return $this;
    }

    /**
     * @param Tag $tag
     * @return Article
     */
    public function removeTag(Tag $tag): Article
    {
        $this->tags->removeElement($tag);
        return $this;
    }

    /**
     * @param null|Category $category
     * @return Article
     */
    public function setCategory(?Category $category): Article
    {
        $this->category = $category;
        return $this;
    }

}
