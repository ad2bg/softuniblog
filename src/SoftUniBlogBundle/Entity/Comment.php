<?php

namespace SoftUniBlogBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Comment
 *
 * @ORM\Table(name="comments")
 * @ORM\Entity(repositoryClass="SoftUniBlogBundle\Repository\CommentRepository")
 */
class Comment
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     */
    private $createdAt;


    /**
     * Many Comments have one Article. This is the owning side.
     * @var Article
     * @ORM\ManyToOne(targetEntity="Article", inversedBy="comments")
     * @ORM\JoinColumn(name="article_id", referencedColumnName="id")
     */
    private $article;

    /**
     * Many Comments have one User. This is the owning side.
     * @var User
     * @ORM\ManyToOne(targetEntity="User", inversedBy="comments")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;


    /**
     * One Comment has Many Replies.
     * @var ArrayCollection|Comment[]
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="parent")
     */
    private $replies;

    /**
     * Many Replies have One Parent.
     * @var Comment
     * @ORM\ManyToOne(targetEntity="Comment", inversedBy="replies")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    private $parent;




    // CONSTRUCTOR
    public function __construct()
    {
        $this->createdAt = new \DateTime('now');
        $this->replies = new ArrayCollection();
    }


    // TO STRING
    public function __toString()
    {
        return $this->title;
    }



    // GETTERS

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return Article
     */
    public function getArticle(): Article
    {
        return $this->article;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return ArrayCollection|Comment[]
     */
    public function getReplies()
    {
        return $this->replies;
    }

    /**
     * @return Comment
     */
    public function getParent(): Comment
    {
        return $this->parent;
    }




    // SETTERS

    /**
     * @param string $title
     * @return Comment
     */
    public function setTitle($title):Comment
    {
        $this->title = htmlspecialchars($title);
        return $this;
    }

    /**
     * @param string|null $description
     * @return Comment
     */
    public function setDescription($description = null): Comment
    {
        $this->description = htmlspecialchars($description);
        return $this;
    }

    /**
     * @param Article $article
     * @return Comment
     */
    public function setArticle(Article $article): Comment
    {
        $this->article = $article;
        return $this;
    }

    /**
     * @param Comment $parent
     * @return Comment
     */
    public function setParent(Comment $parent): Comment
    {
        $this->parent = $parent;
        $parent->addReply($this);
        return $this;
    }

    /**
     * @param Comment $reply
     * @return Comment
     */
    public function addReply($reply): Comment
    {
        $this->replies[] = $reply;
        return $this;
    }




    /**
     * @param User $user
     * @return Comment
     */
    public function setUser(User $user): Comment
    {
        $this->user = $user;
        return $this;
    }

}
