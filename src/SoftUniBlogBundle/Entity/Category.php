<?php

namespace SoftUniBlogBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="categories")
 * @ORM\Entity(repositoryClass="SoftUniBlogBundle\Repository\CategoryRepository")
 */
class Category
{

    private $pathSeparator = '/';
    private $pathSeparatorFormatted = ' >> ';


    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;


    /**
     * @var ?Category
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    private $parent;

    /**
     * @var ArrayCollection|Category[]
     * @ORM\OneToMany(targetEntity="Category", mappedBy="parent")
     */
    private $children;

    /**
     * @var string
     * @ORM\Column(name="path", type="text", unique=true)
     */
    private $path;

    /**
     * @var ArrayCollection|Article[]
     * @ORM\OneToMany(targetEntity="Article", mappedBy="category")
     */
    private $articles;


    // CONSTRUCTOR
    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->articles = new ArrayCollection();
    }


    // TO STRING
    public function __toString()
    {
        return $this->getName();
    }


    // GETTERS

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }


    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name ?? '';
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }


    /**
     * @return null|Category
     */
    public function getParent(): ?Category
    {
        return $this->parent;
    }

    /**
     * @return ArrayCollection|Category[]
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @return bool
     */
    public function hasChildren(): bool
    {
        return count($this->children) > 0;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path ?? '';
    }

    /**
     * @return ArrayCollection|Article[]
     */
    public function getArticles()
    {
        return $this->articles;
    }


    public function getNameWithSpaces()
    {
        $n = substr_count($this->path, $this->pathSeparator) - 1;
        return str_repeat('_', $n * 5) . $this->name;
    }

    public function getPathFormatted()
    {
        return str_replace($this->pathSeparator, $this->pathSeparatorFormatted, $this->path);
    }


    public function getNameTree()
    {
        $n = substr_count($this->path, $this->pathSeparator);
        return str_repeat('..', ($n - 1) * 5) . '|____' . $this->name;
    }


    // SETTERS

    /**
     * @param string $name
     * @return Category
     */
    public
    function setName($name): Category
    {
        $pathSeparator = $this->pathSeparator;
        $this->name = htmlspecialchars($name);
        $this->setPath();
        return $this;
    }

    /**
     * @param string|null $description
     * @return Category
     */
    public
    function setDescription($description = null): Category
    {
        $this->description = htmlspecialchars($description);
        return $this;
    }


    /**
     * @param null|Category $parent
     * @return Category
     */
    public
    function setParent(?Category $parent): Category
    {
        $this->parent = $parent;
        $this->setPath();
        return $this;
    }

    /**
     * @param ArrayCollection|Category[] $children
     * @return Category
     */
    public
    function setChildren($children): Category
    {
        $this->children = $children;
        return $this;
    }

    /**
     * @return Category
     */
    public
    function setPath(): Category
    {
        $path = $this->path;
        $parentPath = null === $this->parent ? '' : $this->parent->getPath();
        $pathShouldBe = $parentPath . $this->pathSeparator . $this->name;
        if ($path !== $pathShouldBe) {
            // update own path
            $this->path = $pathShouldBe;
            // recursively update the paths of all children
            foreach ($this->children as $child) {
                $child->setPath();
            }
        }
        return $this;
    }


//    /**
//     * @param ArrayCollection|Article[] $articles
//     * @return Category
//     */
//    public function setArticles($articles): Category
//    {
//        $this->articles = $articles;
//        return $this;
//    }
}
