<?php

namespace SoftUniBlogBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use SoftUniBlogBundle\Utility\Strings;

/**
 * @ORM\Table(name="tags")
 * @ORM\Entity(repositoryClass="SoftUniBlogBundle\Repository\TagRepository")
 */
class Tag
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var ArrayCollection|Article[]
     * @ORM\ManyToMany(targetEntity="Article", mappedBy="tags")
     */
    private $articles;


    // CONSTRUCTOR
    public function __construct()
    {
        $this->articles = new ArrayCollection();
    }

    public function __toString()
    {
        $name = $this->name;
        $description = $this->description;
        return  Strings::shorten("$name ($description)", 100);
    }


    // GETTERS

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name ?? '';
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @return ArrayCollection|Article[]
     */
    public function getArticles()
    {
        return $this->articles;
    }




    // SETTERS

    /**
     * @param string $name
     * @return Tag
     */
    public function setName($name): Tag
    {
        $this->name = htmlspecialchars($name);
        return $this;
    }

    /**
     * @param string|null $description
     * @return Tag
     */
    public function setDescription($description = null): Tag
    {
        $this->description = htmlspecialchars($description);
        return $this;
    }

//    /**
//     * @param ArrayCollection|Article[] $articles
//     * @return Tag
//     */
//    public function setArticles($articles): Tag
//    {
//        $this->articles = $articles;
//        foreach ($articles as $article){
//            $article->addTag($this);
//        }
//        return $this;
//    }

    /**
     * @param Article $article
     * @return Tag
     */
    public function addArticle($article): Tag
    {
        $this->articles[] = $article;
        $article->addTag($this);
        return $this;
    }

    /**
     * @param Article $article
     * @return Tag
     */
    public function removeArticle($article): Tag
    {
        $this->articles->removeElement($article);
        $article->removeTag($this);
        return $this;
    }
}
