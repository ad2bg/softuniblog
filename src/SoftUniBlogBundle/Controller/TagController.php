<?php

namespace SoftUniBlogBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use SoftUniBlogBundle\Entity\Article;
use SoftUniBlogBundle\Entity\Tag;
use SoftUniBlogBundle\Form\TagType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("tags")
 * @Security("is_granted('ROLE_ADMIN')")
 */
class TagController extends MyController
{
    // INDEX

    /**
     * @Route("/", name="tags_index", methods={"GET"})
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request): Response
    {
        $tags = $this->getAllBy(Tag::class, [], ['name' => 'ASC']);
        $tags = $this->paginate($request, $tags);

        return $this->render('tag/index.html.twig', [
            'articlesTotalCount' => $this->getTotalCount(Article::class),
            'tags' => $tags,
        ]);
    }


    // NEW

    /**
     * @Route("/new", name="tags_new",methods={"GET", "POST"})
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function newAction(Request $request)
    {
        $tag = new Tag();
        $form = $this->createForm(TagType::class, $tag);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if (null !== $this->getOneBy(Tag::class,['name'=>$tag->getName()])) {
                $this->addFlash('error','This tag already exists.');
            } else {

                // save the tag
                $this->saveObject($tag);
                // save the related articles
                foreach ($tag->getArticles() as $article) {
                    $article->addTag($tag);
                    $this->saveObject($article);
                }
                // return
                return $this->redirectToRoute('tags_show', [
                    'id' => $tag->getId(),
                    'goBack' => $request->get('goBack'),
                ]);
            }
        }

        return $this->render('tag/new.html.twig', [
            'articlesTotalCount' => $this->getTotalCount(Article::class),
            'tag' => $tag,
            'form' => $form->createView(),
            'goBack' => $request->get('goBack'),
        ]);
    }


    // SHOW

    /**
     * @Route("/{id}", name="tags_show",methods={"GET"})
     * @param Tag $tag
     * @param Request $request
     * @return Response
     */
    public function showAction(Tag $tag, Request $request)
    {
        $deleteForm = $this->createDeleteForm($tag);

        // sort and paginate the related articles
        $articles = $tag->getArticles()->toArray();
        usort($articles, function (Article $a, Article $b) {
            return strcmp($a->getTitle(), $b->getTitle());
        });
        $articles = $this->paginate($request, $articles);

        return $this->render('tag/show.html.twig', [
            'articlesTotalCount' => $this->getTotalCount(Article::class),
            'tag' => $tag,
            'articles' => $articles,
            'delete_form' => $deleteForm->createView(),
            'goBack' => $request->get('goBack'),
        ]);
    }


    // EDIT

    /**
     * @Route("/{id}/edit", name="tags_edit",methods={"GET", "POST"})
     * @param Request $request
     * @param Tag $tag
     * @return RedirectResponse|Response
     */
    public function editAction(Request $request, Tag $tag)
    {
        $deleteForm = $this->createDeleteForm($tag);

        // remember the existing tag-articles before binding the form data
        $existingArticles = $tag->getArticles()->toArray();

        $editForm = $this->createForm(TagType::class, $tag);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            if (null !== $this->getOneBy(Tag::class,['name'=>$tag->getName()])) {
                $this->addFlash('error',  'This tag already exists.');
            } else {

                $newArticles = $tag->getArticles()->toArray();

                // detach the existing teg-article relations
                foreach ($existingArticles as $article) {
                    $tag->removeArticle($article);
                    $this->saveObject($article);
                }

                // attach the newly selected tag-article relations
                foreach ($newArticles as $article) {
                    $tag->addArticle($article);
                    $this->saveObject($article);
                }
                $this->saveObject($tag);

                $this->addFlash('success',
                    "<strong>$tag</strong> successfully updated");

                return $this->redirectToRoute('tags_edit', [
                    'id' => $tag->getId(),
                    'goBack' => $request->get('goBack'),
                ]);
            }
        }

        return $this->render('tag/edit.html.twig', [
            'articlesTotalCount' => $this->getTotalCount(Article::class),
            'tag' => $tag,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'goBack' => $request->get('goBack'),
        ]);
    }


    // DELETE

    /**
     * @Route("/{id}", name="tags_delete",methods={"DELETE"})
     * @param Request $request
     * @param Tag $tag
     * @return RedirectResponse
     */
    public function deleteAction(Request $request, Tag $tag): RedirectResponse
    {
        $form = $this->createDeleteForm($tag);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->removeObject($tag);
        }

        return $this->redirectToRoute('tags_index');
    }

    /**
     * @param Tag $tag The tag entity
     * @return FormInterface The form
     */
    private function createDeleteForm(Tag $tag): FormInterface
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('tags_delete', ['id' => $tag->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
