<?php

namespace SoftUniBlogBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use SoftUniBlogBundle\Entity\Article;
use SoftUniBlogBundle\Entity\Feeling;
use SoftUniBlogBundle\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/articles")
 * @package SoftUniBlogBundle\Controller
 * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
 */
class FeelingController extends MyController
{

    // LIKE

    /**
     * @Route("/{id}/like", name="article_like", methods={"GET"})
     * @param Request $request
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function likeAction(Request $request, $id): RedirectResponse
    {
        return $this->setFeeling($request, $id, 1);
    }


    // DISLIKE

    /**
     * @Route("/{id}/dislike", name="article_dislike", methods={"GET"})
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function dislikeAction(Request $request, $id): RedirectResponse
    {
        return $this->setFeeling($request, $id, -1);
    }

    /**
     * @param Request $request
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     * @param int $id
     * @param int $type Like = 1  Dislike = -1
     * @return RedirectResponse
     */
    private function setFeeling(Request $request, $id, $type): RedirectResponse
    {
        /** @var Article $article */
        $article = $this->getOneById(Article::class, $id);
        if ($article === null) {
            return $this->redirectToRoute('homepage');
        }

        /** @var User $currentUser */
        $currentUser = $this->getUser();
        if (!$currentUser->isAuthor($article) &&
            !$currentUser->isAdmin()) {
            return $this->redirectToRoute('homepage');
        }

        if ($currentUser->hasFeeling($article) > 0) {
            return $this->redirectToRoute('homepage');
        }

        $feeling = new Feeling();
        $feeling
            ->setType($type)
            ->setUser($currentUser)
            ->setArticle($article);
        $this->saveObject($feeling);

        $this->addFlash('success', 'Thank you for your feedback!');

        return $this->redirectToRoute('article_view', [
            'id' => $id,
            'goBack' => $request->get('goBack')
        ]);
    }

}
