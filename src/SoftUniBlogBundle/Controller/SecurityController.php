<?php

namespace SoftUniBlogBundle\Controller;

use SoftUniBlogBundle\Entity\Article;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends MyController
{
    // LOGIN
    /**
     * @Route("/login", name="security_login")
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    public function loginAction(AuthenticationUtils $authenticationUtils): Response
    {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', [
            'articlesTotalCount' => $this->getTotalCount(Article::class),
            'last_username' => $lastUsername,
            'error' => $error
        ]);
    }

    // LOGOUT

    /**
     * @Route("/logout", name="security_logout")
     * @throws \Exception
     */
    public function logoutAction(): void
    {
        throw new \Exception('Logout failed.');
    }
}
