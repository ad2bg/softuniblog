<?php

namespace SoftUniBlogBundle\Controller;

use SoftUniBlogBundle\Entity\Article;
use SoftUniBlogBundle\Service\Article\ArticleServiceInterface;
use SoftUniBlogBundle\Service\User\UserServiceInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @package SoftUniBlogBundle\Controller
 */
class HomeController extends MyController
{
    /**
     * @var UserServiceInterface
     */
    private $userService;

    /**
     * @var ArticleServiceInterface
     */
    private $articleService;


    /**
     * @param UserServiceInterface $userService
     * @param ArticleServiceInterface $articleService
     */
    public function __construct(
        UserServiceInterface $userService,
        ArticleServiceInterface $articleService)
    {
        $this->userService = $userService;
        $this->articleService = $articleService;
    }


    // INDEX

    /**
     * @Route("/", name="homepage", methods={"GET"})
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request): Response
    {

        // ARTICLES
        /** @var Article[] $articles */
        $articles = $this->getAllBy(Article::class, [], ['title' => 'ASC']);
        $articlesTotalCount = count($articles);
        $articles = $this->paginate($request, $articles);

        [$authors, $tags, $categories,
            $messagesReceived, $messagesSent,
            $untaggedArticlesCount, $uncategorizedArticlesCount] =
            $this->getTotals($request);

        return $this->render('home/index.html.twig', [
            'articlesTotalCount' => $articlesTotalCount,
            'articles' => $articles,
            'authors' => $authors,
            'tags' => $tags,
            'categories' => $categories,
            'messagesReceived' => $messagesReceived,
            'messagesSent' => $messagesSent,
            'untaggedArticlesCount' => $untaggedArticlesCount,
            'uncategorizedArticlesCount' => $uncategorizedArticlesCount,
        ]);

    }


    // BLANK

    /**
     * @Route("/blank", name="home_blank", methods={"GET"})
     * @param Request $request
     * @return Response
     */
    public function clearAction(Request $request): Response
    {
        return $this->render('base.html.twig', [
            'articlesTotalCount' =>  $this->getTotalCount(Article::class),
        ]);
    }



    // ENLARGE IMAGE

    /**
     * @Route("/image",name="enlarge_image", methods={"GET"})
     * @param Request $request
     * @return Response
     */
    public function enlargeImageAction(Request $request): Response
    {
        return $this->render('home/image.html.twig', [
            'articlesTotalCount' => $this->getTotalCount(Article::class),
            'uri' => $request->get('uri'),
            'goBack' => $request->get('goBack')
        ]);
    }
}
//        $projectFolder = realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR;
//        dump($projectFolder);
