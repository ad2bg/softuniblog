<?php

namespace SoftUniBlogBundle\Controller;

use SoftUniBlogBundle\Service\Weather\WeatherServiceInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/weather")
 * @package SoftUniBlogBundle\Controller
 */
class WeatherController extends MyController
{

    /**
     * @var WeatherServiceInterface
     */
    private $weatherService;

    /**
     * @param WeatherServiceInterface $weatherService
     */
    public function __construct(WeatherServiceInterface $weatherService)
    {
        $this->weatherService = $weatherService;
    }


    // SNOW

    /**
     * @Route("/snow", name="weather_snow", methods={"GET"})
     * @param Request $request
     * @return RedirectResponse
     */
    public function snow(Request $request): RedirectResponse
    {
        $this->weatherService->startSnowing();
        return $this->redirect($request->get('currentUri'));
    }


    // SKY

    /**
     * @Route("/sky", name="weather_sky", methods={"GET"})
     * @param Request $request
     * @return RedirectResponse
     */
    public function sky(Request $request): RedirectResponse
    {
        $this->weatherService->goToTheSky();
        return $this->redirect($request->get('currentUri'));
    }


    // BEACH

    /**
     * @Route("/beach", name="weather_beach", methods={"GET"})
     * @param Request $request
     * @return RedirectResponse
     */
    public function beach(Request $request): RedirectResponse
    {
        $this->weatherService->goToTheBeach();
        return $this->redirect($request->get('currentUri'));
    }
}
