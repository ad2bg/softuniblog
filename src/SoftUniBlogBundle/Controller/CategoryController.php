<?php

namespace SoftUniBlogBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use SoftUniBlogBundle\Entity\Article;
use SoftUniBlogBundle\Entity\Category;
use SoftUniBlogBundle\Form\CategoryEditType;
use SoftUniBlogBundle\Form\CategoryType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("categories")
 * @Security("is_granted('ROLE_ADMIN')")
 */
class CategoryController extends MyController
{
    private $pathSeparator = '/';


    // INDEX

    /**
     * @Route("/", name="categories_index", methods={"GET"})
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request): Response
    {
        $categories = $this->getAllBy(Category::class, [], ['path' => 'ASC']);
        $categories = $this->paginate($request, $categories);

        return $this->render('category/index.html.twig', [
            'articlesTotalCount' => $this->getTotalCount(Article::class),
            'categories' => $categories,
        ]);
    }


    // NEW

    /**
     * @Route("/new", name="categories_new",methods={"GET", "POST"})
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function newAction(Request $request)
    {
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $pathSeparator = $this->pathSeparator;
            $name = $category->getName();

            if (strpos($name, $pathSeparator) !== false) {
                $this->addFlash('error',
                    "Category $category cannot contain $pathSeparator");
            } elseif (null !== $this->getOneBy(Category::class, ['path'=>$category->getPath()])) {
                $this->addFlash('error',
                    "Category $category already exists.");
            } else {
                // save the category
                $this->saveObject($category);

                $this->addFlash('success', "Category added: $category");
                return $this->redirectToRoute('categories_show', [
                    'id' => $category->getId(),
                    'goBack' => $request->get('goBack'),
                ]);
            }
        }

        return $this->render('category/new.html.twig', [
            'articlesTotalCount' => $this->getTotalCount(Article::class),
            'category' => $category,
            'form' => $form->createView(),
            'goBack' => $request->get('goBack'),
        ]);
    }


    // SHOW

    /**
     * @Route("/{id}", name="categories_show", methods={"GET"})
     * @param Category $category
     * @param Request $request
     * @return Response
     */
    public function showAction(Category $category, Request $request): Response
    {
        $deleteForm = $this->createDeleteForm($category);

        return $this->render('category/show.html.twig', [
            'articlesTotalCount' => $this->getTotalCount(Article::class),
            'category' => $category,
            'delete_form' => $deleteForm->createView(),
            'goBack' => $request->get('goBack'),
        ]);
    }


    // EDIT

    /**
     * @Route("/{id}/edit", name="categories_edit",methods={"GET", "POST"})
     * @param Request $request
     * @param Category $category
     * @return RedirectResponse|Response
     */
    public function editAction(Request $request, Category $category)
    {
        $deleteForm = $this->createDeleteForm($category);

        // remember the path before binding the form data
        $existingPath = $category->getPath();

        $parent = $category->getParent();
        $id = null === $parent ? null : $parent->getId();

        $editForm = $this->createForm(CategoryEditType::class, $category, [
            'entityManager' => $this->getDoctrine()->getManager(),
            'existingPath' => $existingPath,
            'id' => $id,
        ]);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            $pathSeparator = $this->pathSeparator;
            $name = $category->getName();

            if (strpos($name, $pathSeparator) !== false) {
                $this->addFlash('error',
                    "Category $category cannot contain $pathSeparator");
            } elseif (null !== $this->getOneBy(Category::class, ['path'=>$category->getPath()])) {
                $this->addFlash('error',
                    "Category $category already exists.");
            } else {
                $this->getDoctrine()->getManager()->flush();

                $this->addFlash('success',
                    "Successfully updated category <strong class='text-primary'>$category</strong>.");

                return $this->redirectToRoute('categories_edit', [
                    'id' => $category->getId(),
                    'goBack' => $request->get('goBack'),
                ]);
            }
        }

        return $this->render('category/edit.html.twig', [
            'articlesTotalCount' => $this->getTotalCount(Article::class),
            'category' => $category,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'goBack' => $request->get('goBack'),
        ]);
    }


    // DELETE

    /**
     * @Route("/{id}", name="categories_delete",methods={"DELETE"})
     * @param Request $request
     * @param Category $category
     * @return RedirectResponse
     */
    public function deleteAction(Request $request, Category $category): RedirectResponse
    {
        if ($category->hasChildren()) {
            $this->addFlash('error',
                "Cannot delete category <strong class='text-primary'>$category</strong>" .
                ' because it has some sub-categories.');
        } else {
            try {
                $form = $this->createDeleteForm($category);
                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid()) {
                    $this->removeObject($category);
                }
            } catch (\Exception $e) {
                $this->addFlash('error',
                    "Error deleting category <strong class='text-primary'>$category</strong>");
            }
        }

        return $this->redirectToRoute('categories_index');
    }

    /**
     * @param Category $category The category entity
     * @return FormInterface The form
     */
    private function createDeleteForm(Category $category): FormInterface
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('categories_delete', ['id' => $category->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
