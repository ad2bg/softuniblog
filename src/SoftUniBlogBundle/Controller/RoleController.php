<?php

namespace SoftUniBlogBundle\Controller;

use SoftUniBlogBundle\Entity\Article;
use SoftUniBlogBundle\Entity\Role;
use SoftUniBlogBundle\Entity\User;
use SoftUniBlogBundle\Form\RoleType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("roles")
 */
class RoleController extends MyController
{

    private $protectedRoles = ['ROLE_ADMIN', 'ROLE_USER'];


    // INDEX

    /**
     * @Route("/", name="roles_index", methods={"GET"})
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request): Response
    {
        $roles = $this->getAllBy(Role::class, [], ['name' => 'ASC']);
        $roles = $this->paginate($request, $roles, 1, 5);

        return $this->render('role/index.html.twig', [
            'articlesTotalCount' => $this->getTotalCount(Article::class),
            'roles' => $roles,
            'protectedRoles' => $this->protectedRoles,
        ]);
    }


    // NEW

    /**
     * @Route("/new", name="roles_new", methods={"GET", "POST"})
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function newAction(Request $request)
    {
        $role = new Role();
        $form = $this->createForm(RoleType::class, $role);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $roleName = $role->getName();

            if ($this->getOneBy(Role::class, ['name' => $roleName])) {
                $this->addFlash('error',
                    "Cannot add <strong class='text-primary'>$roleName</strong> because it already exists!");
            } else {

                // save the role
                $this->saveObject($role);
                // save the related users
                foreach ($role->getUsers() as $user) {
                    /** @var User $user */
                    $user->addRole($role);
                    $this->saveObject($user);
                }
                return $this->redirectToRoute('roles_show', [
                    'id' => $role->getId(),
                    'goBack' => $request->get('goBack'),
                ]);
            }
        }

        return $this->render('role/new.html.twig', [
            'articlesTotalCount' => $this->getTotalCount(Article::class),
            'role' => $role,
            'form' => $form->createView(),
            'goBack' => $request->get('goBack'),
        ]);
    }


    // SHOW

    /**
     * @Route("/{id}", name="roles_show", methods={"GET"})
     * @param Role $role
     * @param Request $request
     * @return Response
     */
    public function showAction(Role $role, Request $request): Response
    {
        $deleteForm = $this->createDeleteForm($role);

        // sort and paginate the related users
        $users = $role->getUsers()->toArray();
        usort($users, function (User $a, User $b) {
            return strcmp($a->getFullName(), $b->getFullName());
        });
        $users = $this->paginate($request, $users);

        return $this->render('role/show.html.twig', [
            'articlesTotalCount' => $this->getTotalCount(User::class),
            'role' => $role,
            'protectedRoles' => $this->protectedRoles,
            'users' => $users,
            'delete_form' => $deleteForm->createView(),
            'goBack' => $request->get('goBack'),
        ]);
    }


    // EDIT

    /**
     * @Route("/{id}/edit", name="roles_edit", methods={"GET", "POST"})
     * @param Request $request
     * @param Role $role
     * @return RedirectResponse|Response
     */
    public function editAction(Request $request, Role $role)
    {
        $deleteForm = $this->createDeleteForm($role);

        // remember the existing role-users and name before binding the form data
        $existingUsers = $role->getUsers()->toArray();
        $oldName = $role->getName();

        $editForm = $this->createForm(RoleType::class, $role, [
            'protectedRole' =>  in_array($oldName, $this->protectedRoles,false),
        ]);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            $newName = $role->getName();

            if ($oldName!==$newName && $this->getOneBy(Role::class, ['name' => $newName])) {
                $this->addFlash('error',
                    "Cannot rename to <strong class='text-primary'>$newName</strong> " .
                    'because this name is already taken!');
            } else {

                $newUsers = $role->getUsers()->toArray();

                // detach the existing teg-article relations
                foreach ($existingUsers as $user) {
                    $role->removeUser($user);
                    $this->saveObject($user);
                }

                // attach the newly selected tag-article relations
                foreach ($newUsers as $user) {
                    $role->addUser($user);
                    $this->saveObject($user);
                }
                $this->saveObject($role);

                $this->addFlash('success',
                    "<strong>$role</strong> successfully updated");

                return $this->redirectToRoute('roles_edit', [
                    'id' => $role->getId(),
                    'goBack' => $request->get('goBack'),
                ]);
            }
        }

        return $this->render('role/edit.html.twig', [
            'articlesTotalCount' => $this->getTotalCount(Article::class),
            'role' => $role,
            'protectedRoles' => $this->protectedRoles,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'goBack' => $request->get('goBack'),
        ]);
    }


    // DELETE

    /**
     * @Route("/delete/{id}", name="roles_delete", methods={"DELETE"})
     * @param Request $request
     * @param Role $role
     * @return RedirectResponse
     */
    public function deleteAction(Request $request, Role $role): RedirectResponse
    {
        $roleName = $role->getName();
        $form = $this->createDeleteForm($role);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if (in_array($roleName, $this->protectedRoles, false)) {
                $this->addFlash('warning',
                    "<strong class='text-primary'>$roleName</strong> should not be removed.");
            } else {
                try {
                    $this->removeObject($role);
                    $this->addFlash('success',
                        "<strong class='text-primary'>$roleName</strong> successfully deleted.");
                } catch (\Exception $e) {
                    $this->addFlash('error',
                        "Error deleting <strong class='text-primary'>$roleName</strong>");
                }
            }
        }

        return $this->redirectToRoute('roles_index');
    }

    /**
     * @param Role $role The role entity
     * @return FormInterface
     */
    private function createDeleteForm(Role $role): FormInterface
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('roles_delete', ['id' => $role->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
