<?php

namespace SoftUniBlogBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use SoftUniBlogBundle\Entity\Article;
use SoftUniBlogBundle\Entity\Comment;
use SoftUniBlogBundle\Entity\User;
use SoftUniBlogBundle\Form\CommentType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/articles")
 * @package SoftUniBlogBundle\Controller
 * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
 */
class CommentController extends MyController
{

    // COMMENT

    /**
     * @Route("/comment/{articleId}", name="article_comment", methods={"GET", "POST"})
     * @param Request $request
     * @param int $articleId
     * @return RedirectResponse|Response
     */
    public function commentAction(Request $request, $articleId)
    {
        /** @var Article $article */
        $article = $this->getOneById(Article::class, $articleId);
        if ($article === null) {
            return $this->redirectToRoute('homepage');
        }

        /** @var User $currentUser */
        $currentUser = $this->getUser();

        $comment = new Comment();
        $comment->setArticle($article);
        $comment->setUser($currentUser);

        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->saveObject($comment);

            $this->addFlash('success',
                "Your comment <strong class='text-primary'>$comment</strong> has been added.");

            return $this->redirectToRoute('article_view', [
                'id' => $articleId
            ]);
        }

        return $this->render('comment/create.html.twig', [
            'articlesTotalCount' => $this->getTotalCount(Article::class),
            'form' => $form->createView(),
            'article' => $article,
            'parent' => null,
            'goBack' => $request->get('goBack')
        ]);
    }


    // SUB-COMMENT

    /**
     * @Route("/comment/{articleId}/{parentId}", name="article_subComment", methods={"GET", "POST"})
     * @param Request $request
     * @param int $articleId
     * @param int $parentId
     * @return RedirectResponse|Response
     */
    public function subCommentAction(Request $request, $articleId, $parentId)
    {
        /** @var Article $article */
        $article = $this->getOneById(Article::class, $articleId);
        if ($article === null) {
            return $this->redirectToRoute('homepage');
        }

        /** @var Comment $parent */
        $parent = $this->getOneById(Comment::class, $parentId);
        if ($parent === null) {
            return $this->redirectToRoute('homepage');
        }

        /** @var User $currentUser */
        $currentUser = $this->getUser();

        $comment = new Comment();
        $comment->setArticle($article);
        $comment->setParent($parent);
        $comment->setUser($currentUser);

        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->saveObject($comment);

            $this->addFlash('success',
                "Your comment <strong class='text-primary'>$comment</strong> has been added.");

            return $this->redirectToRoute('article_view', [
                'id' => $articleId
            ]);
        }

        return $this->render('comment/create.html.twig', [
            'articlesTotalCount' => $this->getTotalCount(Article::class),
            'form' => $form->createView(),
            'article' => $article,
            'parent' => $parent,
            'parentId' => $parentId,
            'goBack' => $request->get('goBack')
        ]);
    }
}
