<?php

namespace SoftUniBlogBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use SoftUniBlogBundle\Entity\Article;
use SoftUniBlogBundle\Entity\Message;
use SoftUniBlogBundle\Form\MessageType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("messages")
 * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
 */
class MessageController extends MyController
{

    // INDEX
    /**
     * @Route("/", name="messages_index", methods={"GET"})
     * @Security("is_granted('ROLE_ADMIN')")
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request): Response
    {
        $messages = $this->getAllBy(Message::class,[],['timestamp' => 'DESC']);

        $messages = $this->paginate($request, $messages);

        return $this->render('message/index.html.twig', [
            'articlesTotalCount' => $this->getTotalCount(Article::class),
            'messages' => $messages,
        ]);
    }


    // RECEIVED
    /**
     * @Route("/received", name="messages_received", methods={"GET"})
     * @param Request $request
     * @return Response
     */
    public function receivedAction(Request $request): Response
    {
        $user = $this->getUser();
        $messages = $this->getAllBy(Message::class,['recipient' => $user->getId()],['timestamp' => 'DESC']);

        $messages = $this->paginate($request, $messages);

        return $this->render('message/index.html.twig', [
            'articlesTotalCount' => $this->getTotalCount(Article::class),
            'messages' => $messages,
        ]);
    }



    // SENT
    /**
     * @Route("/sent", name="messages_sent", methods={"GET"})
     * @param Request $request
     * @return Response
     */
    public function sentAction(Request $request): Response
    {
        $user = $this->getUser();
        $messages = $this->getAllBy(Message::class,['sender' => $user->getId()],['timestamp' => 'DESC']);

        $messages = $this->paginate($request, $messages);

        return $this->render('message/index.html.twig', [
            'articlesTotalCount' => $this->getTotalCount(Article::class),
            'messages' => $messages,
        ]);
    }



    // NEW

    /**
     * @Route("/new", name="messages_new", methods={"GET", "POST"})
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function newAction(Request $request)
    {
        $currentUser = $this->getUser();
        if(null === $currentUser){
            $this->redirectToRoute('login',[
               'goBack' => $request->getRequestUri(),
            ]);
        }

        $message = new Message();
        $form = $this->createForm(MessageType::class, $message,[
            'id' => $this->getUser()->getId(),
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $message->setSender($currentUser);
            $this->saveObject($message);
            $this->saveObject($currentUser);

            return $this->redirect($request->get('goBack'));
        }

        return $this->render('message/new.html.twig', [
            'articlesTotalCount' => $this->getTotalCount(Article::class),
            'message' => $message,
            'form' => $form->createView(),
            'goBack' => $request->get('goBack'),
        ]);
    }


    // SHOW

    /**
     * @Route("/{id}", name="messages_show", methods={"GET"})
     * @param Message $message
     * @return Response
     */
    public function showAction(Message $message, Request $request): Response
    {
        $deleteForm = $this->createDeleteForm($message, $request);

        return $this->render('message/show.html.twig', [
            'articlesTotalCount' => $this->getTotalCount(Article::class),
            'message' => $message,
            'delete_form' => $deleteForm->createView(),
            'goBack' => $request->get('goBack'),
        ]);
    }


    // EDIT

    /**
     * @Route("/{id}/edit", name="messages_edit", methods={"GET", "POST"})
     * @param Request $request
     * @param Message $message
     * @return RedirectResponse|Response
     */
    public function editAction(Request $request, Message $message)
    {
        $deleteForm = $this->createDeleteForm($message, $request);
        $editForm = $this->createForm(MessageType::class, $message, [
            'id' => $this->getUser()->getId(),
        ]);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('messages_index', [
                //'id' => $message->getId(),
            ]);
        }

        return $this->render('message/edit.html.twig', [
            'articlesTotalCount' => $this->getTotalCount(Article::class),
            'message' => $message,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'goBack' => $request->get('goBack'),
        ]);
    }


    // DELETE

    /**
     * @Route("/{id}", name="messages_delete",methods={"DELETE"})
     * @param Request $request
     * @param Message $message
     * @return RedirectResponse
     */
    public function deleteAction(Request $request, Message $message): RedirectResponse
    {
        $form = $this->createDeleteForm($message, $request);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->removeObject($message);
        }

        return $this->redirect($request->get('goBack'));
    }

    /**
     * @param Message $message The message entity
     * @param Request $request
     * @return FormInterface
     */
    private function createDeleteForm(Message $message, Request $request): FormInterface
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('messages_delete', [
                'id' => $message->getId(),
                'goBack' => $request->get('goBack'),
            ]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
