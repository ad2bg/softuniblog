<?php

namespace SoftUniBlogBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use SoftUniBlogBundle\Entity\Article;
use SoftUniBlogBundle\Entity\Category;
use SoftUniBlogBundle\Entity\Tag;
use SoftUniBlogBundle\Entity\User;
use SoftUniBlogBundle\Form\ArticleDeleteType;
use SoftUniBlogBundle\Form\ArticleType;
use SoftUniBlogBundle\Service\Article\ArticleServiceInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Translation\Exception\NotFoundResourceException;

/**
 * @Route("/articles")
 * @package SoftUniBlogBundle\Controller
 * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
 */
class ArticleController extends MyController
{
    /**
     * @var ArticleServiceInterface
     */
    private $articleService;

    public function __construct(ArticleServiceInterface $articleService)
    {
        $this->articleService = $articleService;
    }

    // MY ARTICLES

    /**
     * @Route("/my", name="my_articles", methods={"GET"})
     * @param Request $request
     * @return Response
     */
    public function myArticlesAction(Request $request): Response
    {
        // ME AND MY ARTICLES
        $author = $this->getUser();
        $articles = $this->getAllBy(Article::class, ['author' => $author]);
        $articles = $this->paginate($request, $articles);

        [$authors, $tags, $categories,
            $messagesReceived, $messagesSent,
            $untaggedArticlesCount, $uncategorizedArticlesCount] =
            $this->getTotals($request);

        return $this->render('article/list.html.twig', [
            'articlesTotalCount' => $this->getTotalCount(Article::class),
            'author' => $author,
            'articles' => $articles,
            'mainTitle' => 'My Articles',
            'authors' => $authors,
            'tags' => $tags,
            'categories' => $categories,
            'messagesReceived' => $messagesReceived,
            'messagesSent' => $messagesSent,
            'untaggedArticlesCount' => $untaggedArticlesCount,
            'uncategorizedArticlesCount' => $uncategorizedArticlesCount,
        ]);
    }


    // USER ARTICLES

    /**
     * @Route("/user/{id}", name="user_articles", methods={"GET"})
     * @param User $author
     * @param Request $request
     * @return Response
     */
    public function userArticlesAction(User $author, Request $request): Response
    {

        // AUTHOR AND HIS ARTICLES
        $articles = $this->getAllBy(Article::class, ['author' => $author]);
        $articles = $this->paginate($request, $articles);

        [$authors, $tags, $categories,
            $messagesReceived, $messagesSent,
            $untaggedArticlesCount, $uncategorizedArticlesCount] =
            $this->getTotals($request);

        return $this->render('article/list.html.twig', [
            'articlesTotalCount' => $this->getTotalCount(Article::class),
            'author' => $author,
            'articles' => $articles,
            'mainTitle' => 'Articles By ' . $author->getFullName(),
            'authors' => $authors,
            'tags' => $tags,
            'categories' => $categories,
            'messagesReceived' => $messagesReceived,
            'messagesSent' => $messagesSent,
            'untaggedArticlesCount' => $untaggedArticlesCount,
            'uncategorizedArticlesCount' => $uncategorizedArticlesCount,
        ]);
    }


    // TAGGED ARTICLES

    /**
     * @Route("/tag/{tagId}", name="tagged_articles", methods={"GET"})
     * @param int $tagId
     * @param Request $request
     * @return Response
     */
    public function taggedArticlesAction(int $tagId, Request $request): Response
    {
        // TAG AND TAGGED ARTICLES
        /** @var Tag $tag */
        $tag = null;
        $tagName = '';
        if (0 === $tagId) {
            $articles = $this->query(
                'SELECT t, a
                FROM SoftUniBlogBundle:Article a
                LEFT JOIN a.tags t
                WHERE a.tags IS EMPTY');
        } else {
            $tag = $this->getOneById(Tag::class, $tagId);
            if (null === $tag) {
                $this->addFlash('error', "Tag with id $tagId not found");
                return $this->redirectToRoute('homepage');
            }
            $tagName = $tag->getName();
            $articles = $this->query(
                'SELECT t, a
                FROM SoftUniBlogBundle:Article a
                JOIN a.tags t
                WHERE t.id = :tagId', [
                'tagId' => $tagId,
            ]);
        }
        $articles = $this->paginate($request, $articles);

        [$authors, $tags, $categories,
            $messagesReceived, $messagesSent,
            $untaggedArticlesCount, $uncategorizedArticlesCount] =
            $this->getTotals($request);

        return $this->render('article/list.html.twig', [
            'articlesTotalCount' => $this->getTotalCount(Article::class),
            'author' => null,
            'articles' => $articles,
            'mainTitle' => $tagName ?
                "Articles Tagged <strong class='text-primary'>$tagName</strong>." : 'Untagged Articles. ',
            'authors' => $authors,
            'tags' => $tags,
            'categories' => $categories,
            'messagesReceived' => $messagesReceived,
            'messagesSent' => $messagesSent,
            'untaggedArticlesCount' => $untaggedArticlesCount,
            'uncategorizedArticlesCount' => $uncategorizedArticlesCount,
        ]);
    }


    // CATEGORIZED ARTICLES

    /**
     * @Route("/category/{categoryId}", name="categorized_articles", methods={"GET"})
     * @param int $categoryId
     * @param Request $request
     * @return Response
     */
    public function categorizedArticlesAction(int $categoryId, Request $request): Response
    {
        $articlesTotalCount = $this->getTotalCount(Article::class);

        // CATEGORY AND CATEGORIZED ARTICLES
        /** @var Category $category */
        $category = null;
        $categoryName = '';
        if (0 === $categoryId) {
            $articles = $this->query(
                'SELECT c, a
                FROM SoftUniBlogBundle:Article a
                LEFT JOIN a.category c
                WHERE a.category IS NULL');
        } else {
            $category = $this->getOneById(Category::class, $categoryId);
            if (null === $category) {
                $this->addFlash('error', "Category with id $categoryId not found");
                return $this->redirectToRoute('homepage');
            }
            $categoryName = $category->getName();
            $articles = $this->query(
                'SELECT c, a
                FROM SoftUniBlogBundle:Article a
                JOIN a.category c
                WHERE c.id = :categoryId', [
                'categoryId' => $categoryId,
            ]);
        }
        $articles = $this->paginate($request, $articles);

        [$authors, $tags, $categories,
            $messagesReceived, $messagesSent,
            $untaggedArticlesCount, $uncategorizedArticlesCount] =
            $this->getTotals($request);

        return $this->render('article/list.html.twig', [
            'articlesTotalCount' => $articlesTotalCount,
            'author' => null,
            'articles' => $articles,
            'mainTitle' => $categoryName ?
                "Articles Categorized <strong class='text-primary'>$categoryName</strong>." : 'Uncategorized Articles. ',
            'authors' => $authors,
            'tags' => $tags,
            'categories' => $categories,
            'messagesReceived' => $messagesReceived,
            'messagesSent' => $messagesSent,
            'untaggedArticlesCount' => $untaggedArticlesCount,
            'uncategorizedArticlesCount' => $uncategorizedArticlesCount,
        ]);
    }


    // CREATE

    /**
     * @Route("/create",name="article_create", methods={"GET", "POST"})
     * @param Request $request
     * @return Response
     */
    public function createAction(Request $request): Response
    {
        /** @var Article $article */
        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            try {

                $currentUser = $this->getUser();
                $article->setAuthor($currentUser);
                $article->setViewCount(0);
                $this->saveObject($article);

                $title = $article->getTitle();
                $this->addFlash('success',
                    "Article <strong class='text-primary'>$title</strong> successfully created.");

                return $this->redirectToRoute('article_view', ['id' => $article->getId()]);

            } catch (\Exception $exception) {

                $this->addFlash('error',
                    'An error occurred. Please try again.');
            }
        }

        return $this->render('article/create.html.twig', [
            'articlesTotalCount' => $this->getTotalCount(Article::class),
            'form' => $form->createView(),
            'article' => $article,
            'goBack' => $request->get('goBack'),
        ]);
    }

    // VIEW RANDOM

    /**
     * @Route("/random", name="article_view_random", methods={"GET"})
     * @param Request $request
     * @return RedirectResponse
     */
    public function viewRandomArticleAction(Request $request): RedirectResponse
    {
        $articleIds = $this->query('SELECT a.id FROM SoftUniBlogBundle:Article a');
        $randomArticleId = $articleIds[array_rand($articleIds)]['id'];

        return $this->redirectToRoute('article_view', [
            'id' => $randomArticleId,
            'goBack' => $request->get('goBack'),
        ]);
    }


    // VIEW

    /**
     * @Route("/{id}",name="article_view", methods={"GET"})
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function viewAction(Request $request, $id): Response
    {
        /** @var Article $article */
        $article = $this->getOneById(Article::class, $id);
        if ($article === null) {
            return $this->redirectToRoute('homepage');
        }
        $article->setViewCount($article->getViewCount() + 1);

        $rootComments = $this->query(
            'SELECT c FROM SoftUniBlogBundle:Comment c 
            JOIN c.article a
            WHERE a.id = :id AND c.parent is null
            ORDER BY c.id DESC', ['id' => $id]);

        $this->saveObject($article);
        return $this->render('article/view.html.twig', [
            'articlesTotalCount' => $this->getTotalCount(Article::class),
            'article' => $article,
            'rootComments' => $rootComments,
            'goBack' => $request->get('goBack')
        ]);
    }


    // EDIT

    /**
     * @Route("/{id}/edit",name="article_edit", methods={"GET", "POST"})
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function editAction(Request $request, $id): Response
    {
        // Ensure the article exists
        /** @var Article $article */
        $article = $this->getOneById(Article::class, $id);
        if ($article === null) {
            return $this->redirectToRoute('homepage');
        }


        // get current user and ensure that isAuthor OR isAdmin
        /** @var User $currentUser */
        $currentUser = $this->getUser();
        if (!$currentUser->isAuthor($article) &&
            !$currentUser->isAdmin()) {
            return $this->redirectToRoute('homepage');
        }

        $author = $article->getAuthor();

        $category = $article->getCategory();
        $categoryId = null === $category ? null : $category->getId();

        $form = $this->createForm(ArticleType::class, $article, [
            'entityManager' => $this->getDoctrine()->getManager(),
            'categoryId' => $categoryId,
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $article->setAuthor($author);
            $this->mergeObject($article);

            $this->addFlash('success',
                "Article <strong class='text-primary'>$article</strong> has been successfully updated.");

            return $this->redirectToRoute('article_view', [
                'id' => $article->getId(),
                'goBack' => $request->get('goBack'),
            ]);
        }

        return $this->render('article/edit.html.twig', [
            'articlesTotalCount' => $this->getTotalCount(Article::class),
            'form' => $form->createView(),
            'article' => $article,
            'goBack' => $request->get('goBack'),
        ]);
    }

    // DELETE

    /**
     * @Route("/{id}/delete",name="article_delete", methods={"GET", "POST"})
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function deleteAction(Request $request, $id): Response
    {
        /** @var Article $article */
        $article = $this->getOneById(Article::class, $id);
        if ($article === null) {
            return $this->redirectToRoute('homepage');
        }

        /** @var User $currentUser */
        $currentUser = $this->getUser();
        if (!$currentUser->isAuthor($article) &&
            !$currentUser->isAdmin()) {
            return $this->redirectToRoute('homepage');
        }

        $form = $this->createForm(ArticleDeleteType::class, $article);
        $form->handleRequest($request);

        $this->addFlash('success', 'Article deleted successfully.');

        if ($form->isSubmitted() && $form->isValid()) {
            $article->setAuthor($currentUser);
            $this->removeObject($article);
            return $this->redirectToRoute('homepage');
        }

        return $this->render('article/delete.html.twig', [
            'articlesTotalCount' => $this->getTotalCount(Article::class),
            'form' => $form->createView(),
            'article' => $article,
            'goBack' => $request->get('goBack'),
        ]);
    }
}
