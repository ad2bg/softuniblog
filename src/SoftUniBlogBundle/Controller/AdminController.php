<?php

namespace SoftUniBlogBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use SoftUniBlogBundle\Entity\Article;
use SoftUniBlogBundle\Entity\Category;
use SoftUniBlogBundle\Entity\Role;
use SoftUniBlogBundle\Entity\Tag;
use SoftUniBlogBundle\Entity\User;
use SoftUniBlogBundle\Form\AdminChangePasswordType;
use SoftUniBlogBundle\Model\AdminPasswordChangeModel;
use SoftUniBlogBundle\Service\User\UserServiceInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @package SoftUniBlogBundle\Controller
 * @Route("/my-admin")
 * @Security("is_granted('ROLE_ADMIN')")
 */
class AdminController extends MyController
{

    /**
     * @var UserServiceInterface
     */
    private $userService;


    // CONSTRUCTOR

    /**
     * UserController constructor.
     * @param UserServiceInterface $userService
     */
    public function __construct(UserServiceInterface $userService)
    {
        $this->userService = $userService;
    }


    // ADMIN HOME

    /**
     * @Route("/", name="admin_home")
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     * @return Response
     */
    public function homeAction(): Response
    {
        return $this->render('admin/home.html.twig', [
            'articlesTotalCount' => $this->getTotalCount(Article::class),
            'tagsTotalCount' => $this->getTotalCount(Tag::class),
            'categoriesTotalCount' => $this->getTotalCount(Category::class),
            'rolesTotalCount' => $this->getTotalCount(Role::class),
            'usersTotalCount' => $this->getTotalCount(User::class),
        ]);
    }


    // ROLES

    /**
     * @Route("/roles", name="admin_roles")
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     * @param Request $request
     * @return Response
     */
    public function rolesAction(Request $request): Response
    {
        $users = $this->getAll(User::class);
        $roles = $this->getAll(Role::class);

        if ($_POST) {
            $userId = (int)$_POST['user_id'];
            $roleId = (int)$_POST['role_id'];

            /** @var User $user */
            $user = $this->getOneById(User::class, $userId);
            /** @var Role $role */
            $role = $this->getOneById(Role::class, $roleId);
            $roleName = $role->getName();

            if ($user->hasRole($role)) {
                if ($roleName === 'ROLE_ADMIN' && $user->getId() === $this->getUser()->getId()) {
                    $this->addFlash('error',
                        "You cannot remove your own <strong class='text-primary'>$roleName</strong>.");
                } else {
                    $user->removeRole($role);
                    $this->saveObject($user);
                    $this->addFlash('success',
                        "Removed <strong class='text-primary'>$roleName</strong> from $user");
                }
            } else {
                $user->addRole($role);
                $this->saveObject($user);
                $this->addFlash('success',
                    "Added <strong class='text-primary'>$roleName</strong> to $user");
            }

            return $this->redirectToRoute('admin_roles', [
                'selectedUserId' => $userId,
                'selectedRoleId' => $roleId,
                'goBack' => $request->get('goBack'),
            ]);
        }

        return $this->render('admin/roles.html.twig', [
            'articlesTotalCount' => $this->getTotalCount(Article::class),
            'users' => $users,
            'roles' => $roles,
            'selectedUserId' => $request->get('selectedUserId'),
            'selectedRoleId' => $request->get('selectedRoleId'),
            'goBack' =>  $request->get('goBack'),
        ]);
    }


    // INDEX

    /**
     * @Route("/articles", name="admin_articles_index", methods={"GET"})
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request): Response
    {
        // ARTICLES
        /** @var Article[] $articles */
        $articles = $this->getAllBy(Article::class, [], ['viewCount' => 'DESC', 'dateAdded' => 'DESC']);
        $articles = $this->paginate($request, $articles);

        [$authors, $tags, $categories, $untaggedArticlesCount, $uncategorizedArticlesCount] =
            $this->getTotals($request);

        return $this->render('admin/articles-index.html.twig', [
            'articlesTotalCount' => $this->getTotalCount(Article::class),
            'articles' => $articles,
            'authors' => $authors,
            'tags' => $tags,
            'categories' => $categories,
            'untaggedArticlesCount' => $untaggedArticlesCount,
            'uncategorizedArticlesCount' => $uncategorizedArticlesCount,
        ]);
    }

    /**
     * @Route("/password-change", name="admin_change_a_password", methods={"GET", "POST"})
     * @param Request $request
     * @return Response
     */
    public function changeSomeonesPassword(Request $request): Response
    {
        /** @var User[] $users */
        $users = $this->getAllBy(User::class, [], ['email' => 'ASC']);

        $model = new AdminPasswordChangeModel();
        $form = $this->createForm(AdminChangePasswordType::class, $model
        //,['entityManager' => $this->getDoctrine()->getManager(),]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $this->userService->doChangeUserPassword($model->user, $model->password);

                $name = $model->user->getFullName();
                $this->addFlash('success',
                    "<strong class='text-primary'>$name's</strong>" .
                    ' password has been successfully changed.');

                //return $this->redirectToRoute('homepage');

            } catch (\Exception $e) {
                $this->addFlash('error', $e->getMessage());
            }
        }

        return $this->render('admin/change_password.html.twig', [
            'articlesTotalCount' => $this->getTotalCount(Article::class),
            'form' => $form->createView(),
            'users' => $users,
            'goBack' =>  $request->get('goBack'),
        ]);
    }
}
