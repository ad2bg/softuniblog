<?php

namespace SoftUniBlogBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use SoftUniBlogBundle\Entity\Article;
use SoftUniBlogBundle\Entity\User;
use SoftUniBlogBundle\Form\UserPasswordChangeType;
use SoftUniBlogBundle\Form\UserProfileEditType;
use SoftUniBlogBundle\Form\UserProfilePhotoChangeType;
use SoftUniBlogBundle\Form\UserType;
use SoftUniBlogBundle\Model\UserPasswordChangeModel;
use SoftUniBlogBundle\Model\UserProfileUpdateModel;
use SoftUniBlogBundle\Service\User\UserServiceInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/user")
 * @package SoftUniBlogBundle\Controller
 */
class UserController extends MyController
{

    /**
     * @var UserServiceInterface
     */
    private $userService;


    // CONSTRUCTOR

    /**
     * UserController constructor.
     * @param UserServiceInterface $userService
     */
    public function __construct(UserServiceInterface $userService)
    {
        $this->userService = $userService;
    }


    // REGISTER

    /**
     * @Route("/register", name="user_register")
     * @param Request $request
     * @return Response
     */
    public function registerAction(Request $request): Response
    {
        $goBack = $request->get('goBack') ?? '/';

        /** @var User $user */
        $user = new User();

        // create the form and fill in the entity data
        $userPhotosDirectory = $this->getParameter('user_photos_directory');
        $form = $this->createForm(UserType::class, $user, [
            'user_photos_directory' => $userPhotosDirectory
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            try {
                if (!$this->setUserPhoto($request, $user)) {
                    $this->addFlash('error',
                        'This file could not be uploaded. Please, try uploading a smaller file.');
                    return $this->redirectToRoute('login', ['goBack' => $goBack]);
                }
                $this->userService->register($user);

                $name = htmlspecialchars($user->getFullName());
                $this->addFlash('success',
                    "Welcome <strong class='text-primary'>$name</strong>, you are successfully registered." .
                    ' You can now login.');
                return $this->redirectToRoute('security_login');

            } catch (\Exception $ex) {

                $message = $ex->getMessage();
                if (strpos($message, 'Duplicate entry') !== false) {
                    $email = $user->getEmail();
                    $message = "Sorry. An user with email <strong class='text-primary'>$email</strong> already exists.";
                } else {
                    $message = 'Registration error occurred. Please try again.';
                }
                $this->addFlash('error', $message);

                return $this->render('user/register.html.twig', [
                    'articlesTotalCount' => $this->getTotalCount(Article::class),
                    'form' => $form->createView(),
                ]);
            }
        }
        //dump($form->getErrors());
        return $this->render('user/register.html.twig', [
            'articlesTotalCount' => $this->getTotalCount(Article::class),
            'form' => $form->createView(),
            'goBack' => $request->get('goBack') ?? '/',
        ]);
    }


    // GET CURRENT USER FROM DB

    /**
     * @return null|object|User
     */
    private function getCurrentUserFromDb()
    {
        $userId = $this->getUser()->getId(); // take id from session
        return $this->userService->getOneById($userId);
    }


    // CHANGE PASSWORD

    /**
     * @Route("/password/change", name="user_change_password")
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function changePasswordAction(Request $request)
    {
        $goBack = $request->get('goBack') ?? '/';

        $user = $this->getCurrentUserFromDb();
        if (null === $user) {
            $this->redirectToRoute('security_login');
        }

        $model = new UserPasswordChangeModel();
        $form = $this->createForm(UserPasswordChangeType::class, $model);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $this->userService->doChangeUserPassword($user, $model->password);

                $name = $user->getFullName();
                $this->addFlash('success',
                    "<strong class='text-primary'>$name</strong>" .
                    ', you have successfully changed your password.');

                return $this->redirectToRoute('homepage');

            } catch (\Exception $e) {
                $this->addFlash('error', $e->getMessage());
            }
        }

        return $this->render('user/change_password.html.twig', [
            'articlesTotalCount' => $this->getTotalCount(Article::class),
            'form' => $form->createView(),
            'user' => $user,
            'goBack' => $goBack,
        ]);
    }




    // PROFILE

    /**
     * @Route("/profile", name="user_profile")
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     *
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function profileEditAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getCurrentUserFromDb();
        if ($user === null) {
            return $this->redirectToRoute('security_login');
        }

        $model = UserProfileUpdateModel::fromUser($user);

        $form = $this->createForm(UserProfileEditType::class, $model);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            try {

                $user->setFromProfileUpdateModel($model);
                $this->userService->merge($user);

                $name = $user->getFullName();
                $this->addFlash('success',
                    "<strong class='text-primary'>$name</strong>" .
                    ', you have successfully updated your profile.');

                return $this->redirectToRoute('homepage');

            } catch (\Exception $ex) {

                $message = $ex->getMessage();
                if (strpos($message, 'Duplicate entry') !== false) {
                    $email = $user->getEmail();
                    $message = "Sorry. An user with email <strong>$email</strong> already exists.";
                } else {
                    $message = "An error occurred. Please try again. \n\n$message"; // TODO: remove $message
                }
                $this->addFlash('error', $message);

                return $this->render('user/profile.html.twig', ['form' => $form->createView()]);
            }
        }

        return $this->render('user/profile.html.twig', [
            'articlesTotalCount' => $this->getTotalCount(Article::class),
            'form' => $form->createView(),
            'user' => $user,
            'goBack' => $request->get('goBack'),
        ]);
    }


    // CHANGE USER PROFILE PHOTO

    /**
     * @Route("/user/change-profile-photo", name="user_change_profile_photo")
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function changeUserProfilePhoto(Request $request)
    {
        $goBack = $request->get('goBack') ?? '/';

        $user = $this->getCurrentUserFromDb();
        if (null === $user) {
            $this->redirectToRoute('security_login');
        }

        $originalPhoto = $user->getPhoto();
        $userPhotosDirectory = $this->getParameter('user_photos_directory');
        $form = $this->createForm(UserProfilePhotoChangeType::class, $user, [
            'user_photos_directory' => $userPhotosDirectory
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            // (upload and) set the profile photo
            if (!$this->setUserPhoto($request, $user)) {
                $this->addFlash('error',
                    'This file could not be uploaded. Please, try uploading a smaller file.');
                return $this->redirectToRoute('user_change_profile_photo', ['goBack' => $goBack]);
            }

            // update the user
            $this->userService->merge($user);

            // delete the original photo
            if (null !== $originalPhoto) {
                try {
                    $fs = new Filesystem();
                    $fs->remove($userPhotosDirectory . $originalPhoto);
                } catch (\Exception $exception) {
                    // TODO: log something
                }
            }

            // success; redirect back
            $email = $user->getEmail();
            $fullName = $user->getFullName();
            $this->addFlash('success',
                "<strong class='text-primary'>$fullName ($email)</strong>" .
                ' you have successfully updated your profile photo.');
            return $this->redirect($goBack);
        }

        return $this->render('user/change_profile_photo.html.twig', [
            'articlesTotalCount' => $this->getTotalCount(Article::class),
            'form' => $form->createView(),
            'user' => $user,
            'goBack' => $goBack,
        ]);
    }


    // SET USER PHOTO

    /**
     * @param Request $request
     * @param User $user
     * @return bool
     */
    private function setUserPhoto(Request $request, User $user): bool
    {
        $fileName = null;

        /** @var UploadedFile $file */
        $file = $request->files->get('user')['photo'];
        if (null !== $file) {
            if ($file->getError() !== 0) {
                return false;
            }
            $fileName = $this->uploadUserPhoto($file);
        }
        $user->setPhoto($fileName);
        return true;
    }


    // UPLOAD USER PHOTO

    /**
     * @param UploadedFile $file
     * @return null|string
     */
    private function uploadUserPhoto($file): ?string
    {
        // $tempFileName = $form->getData()->getPhoto();
        $originalExtension = $file->getClientOriginalExtension();
        $size = $file->getClientSize();
        $fileName = substr($file->getClientOriginalName(), 0, -\strlen($originalExtension) - 1)
            . '.' . \date('Y-m-d--H-i-s')
            . '.' . substr($this->generateUniqueFileName(), 0, 5)
            . '.' . $file->guessExtension();
        $file->move($this->getParameter('user_photos_directory'), $fileName);
        return $fileName;
    }
}
