<?php

namespace SoftUniBlogRestApiBundle\Controller;

use SoftUniBlogBundle\Entity\Article;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class ApiController extends MyRestController
{
    /**
     * @Route("/", name="rest_api_index")
     */
    public function indexAction(): Response
    {
        return $this->render('api/index.html.twig',[
            'articlesTotalCount' => $this->getTotalCount(Article::class),
        ]);
    }
}
