<?php

namespace SoftUniBlogRestApiBundle\Controller;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use JMS\Serializer\Serializer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MyRestController extends Controller
{
    /**
     * @return Serializer
     */
    protected function getSerializer(): Serializer
    {
        return $this->container->get('jms_serializer');
    }

    /**
     * @param Request $request
     * @param $objectsToSerialize
     * @param int $statusCode
     * @return Response
     */
    protected function serialize(
        Request $request,
        $objectsToSerialize,
        $statusCode = Response::HTTP_OK
    ): Response
    {
        $format = $request->get('format') ?? 'json';
        /** @var Serializer $serializer */
        $serializer = $this->getSerializer();
        $contentType = $format === 'yml'? 'text/yaml' : "application/$format";
        $data = $serializer->serialize($objectsToSerialize, $format);
        $headers = ['content-type' => $contentType];
        return new Response($data, $statusCode, $headers);
    }


    // GET ONE BY ID
    /**
     * @param string $class
     * @param $id
     * @return null|object
     */
    protected function getOneById($class, $id)
    {
        return $this->getDoctrine()->getRepository($class)->find($id);
    }

    // GET ONE BY
    /**
     * @param string $class
     * @param array $kvpArray
     * @return null|object
     */
    protected function getOneBy($class, $kvpArray)
    {
        return $this->getDoctrine()->getRepository($class)->findOneBy($kvpArray);
    }

    // GET ALL
    /**
     * @param $class
     * @return array
     */
    protected function getAll($class)
    {
        return $this->getDoctrine()->getRepository($class)->findAll();
    }

    // GET ALL BY
    /**
     * @param $class
     * @param array $findByKvpArray
     * @param array $sortByKvpArray
     * @param null $limit
     * @param null $offset
     * @return array
     */
    protected function getAllBy($class, $findByKvpArray = [], $sortByKvpArray = [], $limit = null, $offset = null)
    {
        return $this->getDoctrine()->getRepository($class)->findBy($findByKvpArray, $sortByKvpArray, $limit, $offset);
    }

    // QUERY
    /**
     * @param string $sql
     * @param array $paramsKvpArray
     * @return array
     */
    protected function query(string $sql, $paramsKvpArray = [])
    {
        /** @var EntityManager $entityManager */
        $entityManager = $this->getDoctrine()->getManager();
        /** @var Query $query */
        $query= $entityManager->createQuery($sql);
        foreach ($paramsKvpArray as $key => $value) {
            $query->setParameter($key, $value);
        }
        return $query->getResult();
    }


    // GET TOTAL COUNT

    /**
     * @param $class
     * @return int
     */
    protected function getTotalCount($class):int
    {
        return $this->query("SELECT count(x) FROM $class x")[0][1];
    }


    // SAVE OBJECT
    /**
     * @param $object
     */
    protected function saveObject($object)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($object);
        $entityManager->flush();
    }

    // MERGE OBJECT
    /**
     * @param $object
     */
    protected function mergeObject($object)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->merge($object);
        $entityManager->flush();
    }

    // REMOVE OBJECT
    /**
     * @param $object
     */
    protected function removeObject($object)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($object);
        $entityManager->flush();
    }
}
