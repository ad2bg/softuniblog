<?php

namespace SoftUniBlogRestApiBundle\Controller;

use JMS\Serializer\Serializer;
use Nelmio\ApiDocBundle\Annotation\Model;
use phpDocumentor\Reflection\Types\This;
use SoftUniBlogBundle\Entity\Article;
use SoftUniBlogBundle\Entity\User;
use SoftUniBlogBundle\Form\ArticleType;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ArticleController
 * @Route("/articles")
 * @package SoftUniBlogRestApiBundle\Controller
 */
class ArticleRestController extends MyRestController
{
    // GET ALL
    /**
     * @Route("", name="rest_api_articles", methods={"GET"})
     *
     * @SWG\Parameter(
     *     name="format",
     *     in="query",
     *     type="string",
     *     default="json",
     *     description="The requested format of the response: 'json' | 'xml' | 'yml' "
     * )
     * SWG\Tag(name="articles")
     * @SWG\Response(
     *     response=200,
     *     description="Returns all articles",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Article::class))
     *     )
     * )
     *
     * @param Request $request
     * @return Response
     */
    public function getAllAction(Request $request): Response
    {
        return $this->serialize($request, $this->getAll(Article::class));
    }

    // GET ONE
    /**
     * @Route("/{id}", name="rest_api_article", methods={"GET"})
     * @param Request $request
     * @param $id article id
     * @return Response
     *
     * @SWG\Parameter(
     *     name="id",
     *     in="query",
     *     type="int",
     *     description="The requested article id."
     * )
     */
    public function getOneAction(Request $request, $id): Response
    {
        $article = $this->getOneById(Article::class, $id);

        if (null === $article) {
            return $this->serialize(
                $request,
                (object)['error' => 'resource not found'],
                Response::HTTP_NOT_FOUND);
        }

        return $this->serialize($request, $article);
    }

    // CREATE
    /**
     * @Route("", name="rest_api_article_create", methods={"POST"})
     * @param Request $request
     * @return Response
     *
     * @SWG\Parameter(
     *     name="title",
     *     in="body",
     *     type="string",
     *     description="The title for the new article."
     * )
     * @SWG\Parameter(
     *     name="content",
     *     in="body",
     *     type="string",
     *     description="The content for the new article."
     * )
     */
    public function createAction(Request $request): ?Response
    {
        try {

            return $this->serialize(
                $request,
                $this->createNewArticle($request),
                Response::HTTP_CREATED);

        } catch (\Exception $e) {

            return $this->serialize(
                $request,
                (object)['error' => $e->getMessage()],
                Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Creates a new article from request parameters and persists it
     * @param Request $request
     * @return Article - persisted article
     * @throws \Exception
     */
    protected function createNewArticle(Request $request): Article
    {
        $article = new Article();
        $parameters = $request->request->all();
        /** @var Article $persistedType */
        $persistedType = $this->processForm($article, $parameters, 'POST');
        return $persistedType;
    }

    /**
     * @param Article $article
     * @param $params
     * @param string $method
     * @return Article
     * @throws \Exception
     */
    private function processForm($article, $params, $method = 'PUT'): Article
    {
        foreach ($params as $param => $paramValue) {
            if (null === $paramValue || '' === trim($paramValue)) {
                throw new \Exception("Invalid data: $param");
            }
        }
        if (!array_key_exists('authorId', $params)) {
            throw new \Exception('invalid data: authorId');
        }

        $id = $params['authorId'];
        $user = $this->getOneById(User::class, $id);
        if (null === $user) {
            throw new \Exception("invalid user id: $id");
        }

        $form = $this->createForm(ArticleType::class, $article, ['method' => $method]);
        $form->submit($params);

        if ($form->isSubmitted()) {
            $article->setAuthor($user);
            $this->saveObject($article);
            return $article;
        }
        throw new \Exception('submitted data is invalid');
    }

    // EDIT

    /**
     * @Route("/{id}", name="rest_api_article_edit", methods={"PUT"})
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function editAction(Request $request, $id): ?Response
    {
        try {
            /** @var Article $article */
            $article = $this->getOneById(Article::class, $id);
            if (null === $article) {
                // create new article
                $this->createNewArticle($request);
                $statusCode = Response::HTTP_CREATED;
            } else {
                // update existing article
                $this->processForm($article, $request->request->all(), 'PUT');
                $statusCode = Response::HTTP_NO_CONTENT;
            }
            return new Response(null, $statusCode);

        } catch (\Exception $e) {

            return $this->serialize(
                $request,
                (object)['error' => $e->getMessage()],
                Response::HTTP_BAD_REQUEST);

        }
    }

    // DELETE
    /**
     * @Route("/{id}",name="rest_api_article_delete", methods={"DELETE"})
     * @param Request $request
     * @return Response
     */
    public function deleteAction(Request $request, $id): ?Response
    {
        try {

            $article = $this->getOneById(Article::class, $id);
            if (null === $article) {
                $statusCode = Response::HTTP_NOT_FOUND;
            } else {
                $this->removeObject($article);
                $statusCode = Response::HTTP_NO_CONTENT;
            }
            return new Response(null, $statusCode);

        } catch (\Exception $e) {

            return $this->serialize(
                $request,
                (object)['error' => $e->getMessage()],
                Response::HTTP_BAD_REQUEST);

        }
    }
}
