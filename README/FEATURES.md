# FEATURES

## LAYOUT

Fully **responsive**, using Bootstrap 4

### Flexible Base Layout
The ``base.html.twig`` has the following most-important blocks:  

body_id  
header  
body
- flashMessages
- asideLeft | main | asideRight

footer

When the ``asideLeft`` and ``asideRight`` are used, just specify  
number of bootstrap columns to take ( col-**X** ) and 
the ``main`` section will adjust itself.

### Navbar and footer
They are fixed on-screen. This improves the UX as the menu is always accessible without scrolling.
The fore and back colors used in the navbar and footer are parameterized  
and can be easily changed to any color you want.  
See ``header.html.twig``, ``footer.html.twig`` and ``navbar.css``. 


## USER REGISTRATION, LOGIN, LOGOUT

User registration and login with error handling.

**TODO:**  
Activation email

## USER PROFILES

Password change  
Photo upload  
Admin can change anybody's password. Of course this is not a desirable business logic by any means,  
I just did it for fun.

**TODO:**  
Forgot Password email  

## USER ROLES

Roles toggling through the 'my admin' page.  


## BLOG ARTICLES

- CRUD  
- Pagination (using KNP Paginator)  
- All Articles list (paginated)  
- My Articles list (paginated) 
- Articles by Author list (paginated)  
- **'Feelings'** entity which includes Likes and Dislikes and can be extended with 
other feelings like Indifferent, Love, Hate, etc.  
- **Recursive Comments** (only Creation and Read (hierarchical), no Update, no Delete)  
- **Recursive Categories** (an Article belongs to a single Category)(CRUD by Admins)  
- **Tags** (Many-To-Many relationship to the Articles) (CRUD by Admins)  
- The Homepage and the 'My Articles' and 'Articles by Author' lists 
demonstrate the usage of included shared templates (the left and right **sidebars**) 
which in turn hold bootstrap **accordions** which display Tags, Categories and Authors 
(**all separately paginated**) and used ad filters for the Articles.  
 -- Note that **multiple paginators on the same page** are implemented.  
 -- Note that to resolve conflicting behaviour between paginators and accordions 
 (on each pagination accordions would collapse), **Javascript/jQuery** is used to maintain 
 the accordions in the expanded/collapsed state that they were in, on each pagination request.  
- **Random Article** view.  
   
- **WYSIWYG editors** for the Summary and Content fields of the Articles (using FOS CK Editor)  
 -- Note that although not extensively tested to be XSS-proof, it looks OK.      

- **Messages** - view the messages sent / received in the sidebar on the home page  
or in the list of own or another user articles.



## FRONT-END AND DEV FEATURES

- Used **Services layer**.
- Also used **MyController** which is extended by the other controllers and holds some frequently used methods.
- Similarly, **MyRepository** is extended by the other repositories.
- Used Encore to pre-process **SASS**.  
- Heavy use of **snippets** (partial Twig templates) to minimize code duplication.
- **Fancy file-upload input field**, used as a snippet template. 
- Many buttons and other UI elements also used as snippets.
- Background animation using CSS (snow).  
- By means of the '**use**' Twig tag we have a slightly more complex template structure:  
  Some buttons and other UI elements can be injected in the Navbar.  
- Using Twig **macros** on some elements (tables).
  
- Numerous **tooltips** and Font Awesome **icons** on most of the buttons/links.
- Some of the tooltips are html-styled and display calculated results.  

- About 40 sample articles (mostly content from Wikipedia.com) 
and they are mostly tagged/categorized.

----
