# HOW TO RUN THE SOFTUNI BLOG

## 1. Create the database
Run your MySQL server,  
then run one of the scripts in the sql folder:  
``heidi.sql`` or ``phpMyAdmin.sql``.

## 2. Install the dependencies
To generate the vendor folder, run the ``composer install`` command.

## 3. Run the dev server
Use the ``php bin/console server:run`` command.  
Navigate to ``http://127.0.0.1:8000``
